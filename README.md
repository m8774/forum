# Forum

## [Check out the Forum on Azure!](https://theforum00.azurewebsites.net/)
Contact us for the admin password to see all the functionalities!

## This is an universal forum software

It has the following features:

- RESTful WEB API
- Swagger
- MVC web app
- Registering users
- User, Moderator, Admin Roles
- Admins can Block and Ban users
- Admins and Moderators can delete and edit posts
- Users have profiles with a picture
- Users can edit their profiles 
- Admins can edit all profiles
- Users can post posts
- Posts have tags and can be sorted by them
- Users can search posts
- Users can post comments
- Users can edit and delete their comments and posts
- Users can be searched by username, firstname and email
- And many more features...
