﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Forum.Data;
using Microsoft.EntityFrameworkCore;
using Forum.Models;
using Forum.Repositories;
using Moq;
using System.Linq;


namespace Forum.Test.Repositories
{
    [TestClass]
    public class PostsRepositoryShould
    {
        private DbContextOptions<ApplicationContext> options;

        [TestInitialize]
        public void Initialize()
        {
            string databaseName = "TestDb";
            this.options = new DbContextOptionsBuilder<ApplicationContext>()
                .UseInMemoryDatabase(databaseName: databaseName)
                .Options;

            var context = new ApplicationContext(this.options);

            context.Users.AddRange(ModelBuilderExtentions.GetUsers());
            context.Posts.AddRange(ModelBuilderExtentions.GetPosts());
            context.Comments.AddRange(ModelBuilderExtentions.GetComments());

            context.SaveChanges();
        }

        [TestCleanup]
        public void Cleanup()
        {
            var context = new ApplicationContext(this.options);
            context.Database.EnsureDeleted();
        }
        
        [TestMethod]
        public void GetPostById_Should()
        {
            // Arrange
            var expectedPost = new Post()
            {
                Id = 1,
                Title = "Twitter",
                Content = "I bought twitter",
                AuthorId = 2,
                UpVotes = 0,
                DownVotes = 0
            };

            // Act
            var context = new ApplicationContext(this.options);

            var sut = new PostsRepository(context);

            var actualPost = sut.Get(1);
            //Assert
            Assert.AreEqual(expectedPost.Id, actualPost.Id);
            Assert.AreEqual(expectedPost.Title, actualPost.Title);
            Assert.AreEqual(expectedPost.Content, actualPost.Content);
            Assert.AreEqual(expectedPost.AuthorId, actualPost.AuthorId);
            Assert.AreEqual(expectedPost.UpVotes, actualPost.UpVotes);
            Assert.AreEqual(expectedPost.DownVotes, actualPost.DownVotes);
            Assert.AreEqual(1, actualPost.Comments.Count);
        }
    }
}
