﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Forum.Data;
using Microsoft.EntityFrameworkCore;
using Forum.Models;
using Forum.Repositories;


namespace Forum.Test.Repositories
{
    [TestClass]
    public class UsersRepositoryShould
    {
        private DbContextOptions<ApplicationContext> options;

        [TestInitialize]
        public void Initialize()
        {
            string databaseName = "TestDb";
            this.options = new DbContextOptionsBuilder<ApplicationContext>()
                .UseInMemoryDatabase(databaseName: databaseName)
                .Options;

            var context = new ApplicationContext(this.options);

            context.Users.AddRange(ModelBuilderExtentions.GetUsers());
            context.Posts.AddRange(ModelBuilderExtentions.GetPosts());
            context.Comments.AddRange(ModelBuilderExtentions.GetComments());

            context.SaveChanges();
        }

        [TestCleanup]
        public void Cleanup()
        {
            var context = new ApplicationContext(this.options);
            context.Database.EnsureDeleted();
        }
        
        [TestMethod]
        public void GetUserById_Should()
        {
            // Arrange
            var expectedUser = new User()
            {
                Id = 1,
                FirstName = "I am Admin",
                LastName = "Adminov",
                Username = "admin",
                Password = "admin",
                Email = "admin@localhost.com",
                Role = Forum.Enums.Roles.Admin
            };
            // Act
            var context = new ApplicationContext(this.options);

            var sut = new UsersRepository(context);

            var actualUser = sut.Get(1);

            // Assert
            Assert.AreEqual(expectedUser.Id, actualUser.Id);
            Assert.AreEqual(expectedUser.FirstName, actualUser.FirstName);
            Assert.AreEqual(expectedUser.LastName, actualUser.LastName);
            Assert.AreEqual(expectedUser.Username, actualUser.Username);
            Assert.AreEqual(expectedUser.Password, actualUser.Password);
            Assert.AreEqual(expectedUser.Email, actualUser.Email);
            Assert.AreEqual(expectedUser.Role, actualUser.Role);

        }
    }
}
