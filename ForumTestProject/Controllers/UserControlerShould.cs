﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Forum.Data;
using Microsoft.EntityFrameworkCore;
using Forum.Models;
using Forum.Services.Interfaces;
using Moq;
using System.Linq;
using Microsoft.Extensions.Logging;
using Forum.Controllers.Helpers;
using Forum.Models.Mappers;
using Microsoft.AspNetCore.Mvc;
using Forum.Controllers.Api;

namespace Forum.Test.Controllers
{
    [TestClass]
    public class UserControlerShould
    {
        private DbContextOptions<ApplicationContext> options;

        [TestInitialize]
        public void Initialize()
        {
            string databaseName = "TestDb";
            this.options = new DbContextOptionsBuilder<ApplicationContext>()
                .UseInMemoryDatabase(databaseName: databaseName)
                .Options;

            var context = new ApplicationContext(this.options);

            context.Users.AddRange(ModelBuilderExtentions.GetUsers());
            context.Posts.AddRange(ModelBuilderExtentions.GetPosts());
            context.Comments.AddRange(ModelBuilderExtentions.GetComments());

            context.SaveChanges();
        }

        [TestCleanup]
        public void Cleanup()
        {
            var context = new ApplicationContext(this.options);
            context.Database.EnsureDeleted();
        }
        
        [TestMethod]
        public void UsersControllerGetById_Should()
        {
            // Arrange
            var context = new ApplicationContext(this.options);
            var expectedUser = context.Users.Where(u => u.Id == 1).FirstOrDefault();

            var loggedUser = new User()
            {
                Id = 1,
                FirstName = "I am Admin",
                LastName = "Adminov",
                Username = "admin",
                Password = "admin",
                Email = "admin@localhost.com",
                Role = Forum.Enums.Roles.Admin
            };

            var usersService = new Mock<IUsersService>();
            usersService.Setup(service => service.Get(1, loggedUser))
                .Returns(expectedUser);
            var logMock = new Mock<ILogger<UsersController>>();
            var authMock = new Mock<AuthenticationHelper>();
            var mapperMock = new Mock<UserMapper>();
            var uMapperMock = new Mock<UserUpdateMapper>();

            var controller = new UsersController(logMock.Object, usersService.Object, authMock.Object, mapperMock.Object, uMapperMock.Object);

            var actionResult = controller.Get(1, "admin", "admin") as ObjectResult;

            int statusCode = actionResult.StatusCode.Value;

            Assert.AreEqual(200, statusCode);

            User user = actionResult.Value as User;

            Assert.AreEqual(expectedUser.Username, user.Username);
        }
    }
}
