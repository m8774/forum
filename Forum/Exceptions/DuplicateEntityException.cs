﻿using System;

namespace Forum.Exceptions
{
    public class DuplicateEntityException : ApplicationException
    {
        public DuplicateEntityException(string message) : base(message)
        {
        }
    }
}
