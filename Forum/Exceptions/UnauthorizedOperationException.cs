﻿using System;

namespace Forum.Exceptions
{
	public class UnauthorizedOperationException : ApplicationException
	{
		public UnauthorizedOperationException(string message)
			: base(message)
		{
		}
	}
}
