﻿using System;

namespace Forum.Exceptions
{
    public class ResourceUnvailableException : ApplicationException
    {
        public ResourceUnvailableException(string nameOfEntity) : base(nameOfEntity)
        { 
                
        }
    }
}
