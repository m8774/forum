﻿using Forum.Models;
using Forum.Repositories.Interfaces;
using Forum.Services.Interfaces;
using System.Collections.Generic;
using Forum.Exceptions;
using System.Linq;
using System;

namespace Forum.Services
{
    public class UsersService : IUsersService
    {
        private readonly IUsersRepository repo;
        private readonly IImageRepository imageRepo;
        private readonly IPhoneRepository phoneRepo;




        public UsersService(IUsersRepository repository, IImageRepository imageRepo, IPhoneRepository phoneRepo)
        {
            this.repo = repository;
            this.imageRepo = imageRepo;
            this.phoneRepo = phoneRepo;
        }

        public User Create(User user)
        {

            var u = this.repo.Get(user.Username);


            if (u == null)
            {
                user.Role = Enums.Roles.User;

                //creates the user
                var newUser = this.repo.Create(user);

                //adds default photo to user
                newUser.Picture = imageRepo.Create(newUser);
                this.repo.Update(newUser);

                return newUser;
            }

            throw new DuplicateEntityException($"User {user.Username} allready exist");
        }

        public void Delete(int id, User user)
        {
            if (user == null)
            {
                throw new UnauthorizedOperationException("You must be logged in to perform this action");
            }
            var u = this.repo.Get(id);

            if (user.Role != Enums.Roles.Admin && user.Id != u.Id)
            {
                throw new UnauthorizedOperationException("Only the owner of the account and the admins can delete the account");
            }

            this.repo.Delete(id);
        }

        public User Get(int id, User user)

        {

            if (user == null)
            {
                throw new UnauthorizedOperationException("You must be logged in to perform this action");
            }

            if (user.Role != Enums.Roles.Admin && user.Id != user.Id)
            {
                throw new UnauthorizedOperationException("Only admins can see other users");
            }
            var u = this.repo.Get(id);

            if (u == null)
            {
                throw new EntityNotFoundException();
            }

            var base64EncodedBytes = System.Convert.FromBase64String(u.Picture.ImagePath);
            u.Picture.ImagePath = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
           
            return u;
        }

        public PaginatedList<User> Get(UserQueryParameters filterParameters, User user)
        {
            IEnumerable<User> paginatedUsers;

            if (user == null)
            {
                throw new UnauthorizedOperationException("You must be logged in to perform this action");
            }
          
            var returnedUsers = this.repo.Get(filterParameters);

            if (returnedUsers == null)
            {
                throw new EntityNotFoundException();
            }

            int totalPages = (int)Math.Ceiling((double)returnedUsers.Count() / filterParameters.PageSize);

            paginatedUsers = returnedUsers.Skip((filterParameters.PageNumber - 1) * filterParameters.PageSize).Take(filterParameters.PageSize).ToList();

            foreach (var u in returnedUsers)
            {
                var base64EncodedBytes = System.Convert.FromBase64String(u.Picture.ImagePath);
                u.Picture.ImagePath = System.Text.Encoding.UTF8.GetString(base64EncodedBytes);
            }
            return new PaginatedList<User>(paginatedUsers, totalPages, filterParameters.PageNumber);
        }

        public User Get(string username)
        {

            var u = this.repo.Get(username);

            if (u == null)
            {
                throw new EntityNotFoundException();
            }

            return u;
        }

        public int GetCount()
        {
            return this.repo.Get().Count();
        }

        public User Update(int id, User updateData, User user)
        {

            if (user == null)
            {
                throw new UnauthorizedOperationException("You must be logged in to perform this action");

            }
            var userToUpdate = this.repo.Get(id);


            if (user.Role == Enums.Roles.Admin && updateData.Role != Enums.Roles.Null)
            {
                userToUpdate.Role = updateData.Role;

            }
            
            if (updateData.Role == Enums.Roles.Moderator || updateData.Role == Enums.Roles.Admin)
            {
                userToUpdate.Phone = phoneRepo.Create(updateData.Phone.PhoneNumber, userToUpdate);
            }



            if (userToUpdate.Username == user.Username || user.Role == Enums.Roles.Admin)
            {

                if (!string.IsNullOrEmpty(updateData.FirstName))
                {
                    userToUpdate.FirstName = updateData.FirstName;
                }

                if (!string.IsNullOrEmpty(updateData.LastName))
                {
                    userToUpdate.LastName = updateData.LastName;
                }

                if (!string.IsNullOrEmpty(updateData.Password))
                {
                    userToUpdate.Password = updateData.Password;
                }

                if (!string.IsNullOrEmpty(updateData.Email))
                {
                    userToUpdate.Email = updateData.Email;
                }

                if (!string.IsNullOrEmpty(updateData.Picture.ImagePath))
                {
                    userToUpdate.Picture = imageRepo.Create(userToUpdate, updateData.Picture.ImagePath);
                }
            }
            else
            {
                throw new UnauthorizedOperationException("Only the owner of the profile or Admins can update their profile");
            }
            userToUpdate.DateModified = System.DateTime.Now;
            return this.repo.Update(userToUpdate);
        }
    }
}
