﻿using Forum.Models;
using Forum.Models.Dtos;
using System.Collections.Generic;

namespace Forum.Services.Interfaces
{
    public interface IPostsService
	{
		Post Get(int id, User user);
		PaginatedList<Post> Get(PostQueryParameters filterParameters, User user);
		Post Create(Post post, User user);
		Post Update(int id, Post post, User user);
		void Delete(int id, User user);
        void UpVote(int id, User user);
        void DownVote(int id, User user);
        int GetCount();
        ICollection<Tag> AddTagsToPosts(int postId, ICollection<TagDto> tags);
    }
}