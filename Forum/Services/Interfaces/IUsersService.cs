﻿using Forum.Models;
using System.Collections.Generic;

namespace Forum.Services.Interfaces
{
    public interface IUsersService
    {
        User Get(int id, User user);
        User Get(string username);
        PaginatedList<User> Get(UserQueryParameters filterParameters, User user);
        User Create(User user);
        User Update(int id, User user, User user1);
        void Delete(int id, User user);
        int GetCount();
    }
}