﻿using Forum.Models;
using System.Collections.Generic;

namespace Forum.Services.Interfaces
{
    public interface ICommentsService
	{
		IEnumerable<Comment> Get();
		Comment Get(int id);
		Comment Get(int id, User user);
		Comment Get(User author);
		IEnumerable<Comment> Get(CommentQueryParameters filterParameters);
		Comment Create(Comment comment, User user);
		Comment Update(int id, Comment commentToUpdate, User user);
		void Delete(int id, User user);
	}
}