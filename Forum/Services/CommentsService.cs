﻿using Forum.Models;
using Forum.Repositories.Interfaces;
using Forum.Services.Interfaces;
using System.Collections.Generic;
using Forum.Exceptions;

namespace Forum.Services
{
    public class CommentsService : ICommentsService
    {
        private readonly ICommentsRepository repo;
        public CommentsService(ICommentsRepository repository)
        {
            this.repo = repository;
        }

        public Comment Create(Comment comment, User user)
        {

            if (user == null)
            {
                throw new UnauthorizedOperationException("You need to be logged in to comment");
            }
            if (user.Role == Enums.Roles.Blocked)
            {
                throw new UnauthorizedOperationException("You cannot comment. You have been blocked");
            }

            return this.repo.Create(comment);
        }

        public void Delete(int id, User user)
        {
            var c = this.repo.Delete(id);

            if (c == null)
            {
                throw new EntityNotFoundException();
            }

            if (user.Id != c.AuthorId && user.Role != Enums.Roles.Admin)
            {
                throw new UnauthorizedOperationException("Only the author and the admin can delete comments");
            }

            this.repo.Delete(id);
        }

        public IEnumerable<Comment> Get()
        {
            return new List<Comment>(this.repo.Get());
        }

        public Comment Get(int id)
        {
            var c = this.repo.Get(id);

            if (c == null)
            {
                throw new EntityNotFoundException();
            }

            return c;
        }
        public Comment Get(int id, User user)
        {
            var c = this.repo.Get(id);

            if (c == null)
            {
                throw new EntityNotFoundException();
            }

            if (user.Role == Enums.Roles.Banned)
            {
                throw new UnauthorizedOperationException("You cannot see this comment");
            }

            return c;
        }
        public Comment Get(User author)
        {
            return this.repo.Get(author);
        }

        public IEnumerable<Comment> Get(CommentQueryParameters filterParameters)
        {
            var c = repo.Get(filterParameters);

            if (c == null)
            {
                throw new EntityNotFoundException();
            }

            return c;

        }

        public Comment Update(int cid, Comment commentToUpdate, User user)
        {
            var comment = this.Get(cid, user);

            if (comment == null)
            {
                throw new EntityNotFoundException();
            }

            if (comment.AuthorId == user.Id || user.Role == Enums.Roles.Admin || user.Role == Enums.Roles.Moderator)
            {
                comment.Content = commentToUpdate.Content;

                return this.repo.Update(cid, comment);
            }
            else
            {
                throw new UnauthorizedOperationException("Only the author, the moderators and the admins can update comments");
            }
        }
    }
}
