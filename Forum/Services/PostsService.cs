﻿using Forum.Models;
using Forum.Repositories.Interfaces;
using Forum.Services.Interfaces;
using System.Collections.Generic;
using Forum.Exceptions;
using System.Linq;
using Forum.Models.Dtos;
using System;
using Microsoft.EntityFrameworkCore;

namespace Forum.Services
{
    public class PostsService : IPostsService
    {
        private readonly IPostsRepository repo;
        private readonly ITagsRepository tagsRepo;
        private readonly IUsersRepository userRepo;

        public PostsService(IPostsRepository repository, ITagsRepository tagsRepo, IUsersRepository userRepo)
        {
            this.repo = repository;
            this.tagsRepo = tagsRepo;
            this.userRepo = userRepo;
        }

        public ICollection<Tag> AddTagsToPosts(int postId, ICollection<TagDto> tags)
        {
            var p = this.repo.Get(postId);

            HashSet<TagDto> tagsToAdd = new HashSet<TagDto>(tags);

            foreach (var t in tagsToAdd)
            {
                var tag = tagsRepo.Create(t.Tag);

                if (p.Tags == null)
                {
                    p.Tags = new List<Tag>();

                }

                p.Tags.Add(tag);

                //тука се престарах малко. Фреймурка си го прави автоматично
                //tagsRepo.Update(tag.Name, p);

            }

            repo.Update(postId, p);

            return p.Tags;
        }

        public Post Create(Post post, User user)
        {
            var newPost = this.repo.Get().Where(p => p.Title == post.Title).FirstOrDefault();
            if (newPost != null)
            {
                throw new DuplicateEntityException("Post already exists");
            }
            if (user.Role == Enums.Roles.Blocked)
            {
                throw new UnauthorizedOperationException("You have been blocked. You can only view posts but not create new");
            }

            post.AuthorId = user.Id;
            return repo.Create(post);
        }

        public void Delete(int id, User user)
        {
            if (user == null)
            {
                throw new UnauthorizedOperationException("You must be logged in to perform this action");
            }
            var p = this.repo.Get(id);

            if (p == null)
            {
                throw new EntityNotFoundException();
            }

            if (p.AuthorId == user.Id || user.Role == Enums.Roles.Admin)
            {
                this.repo.Delete(id);

            }
            else
            {
                throw new UnauthorizedOperationException("Only the author and the admins can delete post");
            }
        }



        public Post Get(int id, User user)
        {
            if (user == null)
            {
                throw new UnauthorizedOperationException("To view this content you have to be logged in first");

            }

            if (user.Role == Enums.Roles.Banned)
            {
                throw new UnauthorizedOperationException("You have been banned. You can't view post or make posts. Please contact the admins.");
            }

            var post = this.repo.Get(id);

            if (post == null)
            {
                throw new EntityNotFoundException();
            }

            return post;
        }

        public PaginatedList<Post> Get(PostQueryParameters filterParameters, User user)
        {
            IEnumerable<Post> paginatedPosts;
            if (user == null)
            {
                if (filterParameters.PageNumber == 1 &&
                        filterParameters.PageSize == 10 &&
                        filterParameters.SortBy == "comments" &&
                        filterParameters.OrderBy == "descending")
                {
                    var posts = this.repo.Get(filterParameters);
                    paginatedPosts = (IEnumerable<Post>)posts.OrderByDescending(p => p.Comments.Count).Take(10).AsAsyncEnumerable();
                    return new PaginatedList<Post>(paginatedPosts, 1, 1);
                }
                else if (filterParameters.PageNumber == 1 &&
                        filterParameters.PageSize == 10 &&
                        filterParameters.SortBy == "created" &&
                        filterParameters.OrderBy == "descending")
                {
                    var posts = this.repo.Get(filterParameters);
                    paginatedPosts = (IEnumerable<Post>)posts.OrderByDescending(p => p.DateCreated).Take(10).AsAsyncEnumerable();
                    return new PaginatedList<Post>(paginatedPosts, 1, 1);
                }
                else
                {
                    throw new UnauthorizedOperationException("You need to be logged in to be able to use other query parameters");
                }
            }
            
            var res = this.repo.Get(filterParameters);
            int totalPages = (int)Math.Ceiling((double)res.Count() / filterParameters.PageSize);

            paginatedPosts = res.Skip((filterParameters.PageNumber - 1) * filterParameters.PageSize).Take(filterParameters.PageSize).ToList();

            return new PaginatedList<Post>(paginatedPosts, totalPages, filterParameters.PageNumber);


        }

        public int GetCount()
        {
            return this.repo.Get().Count();
        }

        public Post Update(int id, Post post, User user)
        {
            var p = this.repo.Get(id);

            if (p.AuthorId == user.Id || user.Role == Enums.Roles.Admin || user.Role == Enums.Roles.Moderator)
            {

                return this.repo.Update(id, post);
            }
            else
            {
                throw new UnauthorizedOperationException($"Only the author or Admins and Moderators can chage the post");

            }
        }

        public void UpVote(int id, User user)
        {
            if (user == null)
            {
                throw new UnauthorizedOperationException("You must be logged in to be able to vote");
            }
          

            Post post = this.repo.Get(id);

            if (user.UpVotes == null)
            {
                user.UpVotes = new List<Post>();
            }
            if (user.DownVotes == null)
            {
                user.DownVotes = new List<Post>();
            }
            if (user.UpVotes.Contains(post))
            {
                return;
            }

            if (user.DownVotes.Contains(post))
            {
                user.DownVotes.Remove(post);
                post.DownVotes--;
            }

            post.UpVotes++;
            user.UpVotes.Add(post);

            this.repo.Update(id, post);
            this.userRepo.Update(user);


        }

        public void DownVote(int id, User user)
        {
            if (user == null)
            {
                throw new UnauthorizedOperationException("You must be logged in to be able to vote");
            }

            Post post = this.repo.Get(id);

            if (user.UpVotes == null)
            {
                user.UpVotes = new List<Post>();
            }
            if (user.DownVotes == null)
            {
                user.DownVotes = new List<Post>();
            }
            if (user.DownVotes.Contains(post))
            {
                return;
            }

            if (user.UpVotes.Contains(post))
            {
                user.UpVotes.Remove(post);

                post.UpVotes--;
            }

            post.DownVotes++;
            user.DownVotes.Add(post);

            this.repo.Update(id, post);
            this.userRepo.Update(user);
        }
    }
}
