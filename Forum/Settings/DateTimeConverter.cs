﻿using System.Diagnostics;
using System.Text.Json;
using System;
using System.Text.Json.Serialization;

namespace Forum.Settings
{
    public class DateTimeConverter : JsonConverter<DateTime>
    {
        private readonly string dateTimeFormat;

        public DateTimeConverter(string dtf)
        {
            dateTimeFormat = dtf;
        }
        public override DateTime Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            Debug.Assert(typeToConvert == typeof(DateTime));
            return DateTime.Parse(reader.GetString());
        }

        public override void Write(Utf8JsonWriter writer, DateTime value, JsonSerializerOptions options)
        {
            writer.WriteStringValue(value.ToUniversalTime().ToString(dateTimeFormat));
        }
    }
}
