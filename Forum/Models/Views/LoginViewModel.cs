﻿using System.ComponentModel.DataAnnotations;

namespace Forum.Models.Views
{
    public class LoginViewModel
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string Password { get; set; }
    }
}
