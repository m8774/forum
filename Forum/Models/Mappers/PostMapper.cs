﻿using Forum.Models.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Forum.Models.Mappers
{
    public class PostMapper
    {
        public Post ConvertToModel(PostDto dto, User author)
        {
            Post model = new();
            model.AuthorId = author.Id;
            model.Content = dto.Content;
            model.Title = dto.Title;

            return model;
        }

        public IEnumerable<PostResponseDto> ConvertToResponse(IEnumerable<Post> posts)
        {
            PostResponseDto[] response = new PostResponseDto[posts.Count()];
            foreach (var post in posts)
            {
                ICollection<TagDto> responseTags = new List<TagDto>();

                foreach (var tag in post.Tags)
                {
                    responseTags.Add(new TagDto() { Tag = tag.Name });
                }

                response[posts.ToList().IndexOf(post)] = new PostResponseDto()
                {
                    PostId = post.Id,
                    Title = post.Title,
                    Content = post.Content,
                    AuthorId = post.AuthorId,
                    AuthorUsername = post.Author.Username,
                    UpVotes = post.UpVotes,
                    DownVotes = post.DownVotes,
                    Comments = (ICollection<CommentResponseDto>)new CommentMapper().ConvertToResponse(post.Comments),
                    Tags = responseTags,
                    CreatedOn = post.DateCreated.ToString(),
                    ModifiedOn = post.DateModified.ToString()
                };
            }
            return response;
        }
    }
}