﻿using Forum.Models.Dtos;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Forum.Models.Mappers
{
    public class UserMapper
    {
		public User ConvertToModel(UserDto dto)
		{
			User model = new User();
			model.FirstName = dto.FirstName;
            model.LastName = dto.LastName;
            model.Email = dto.Email;
            model.Password = dto.Password;
            model.Username = dto.Username;
            
			return model;
		}

        internal IEnumerable<UserResponseDto> ConvertToResponse(IEnumerable<User> users)
        {
            UserResponseDto[] response = new UserResponseDto[users.Count()];
            foreach (var user in users)
            {
                response[users.ToList().IndexOf(user)] = new UserResponseDto()
                {
                    RegisteredOn = user.DateCreated.ToString(),
                    UserId = user.Id,
                    Username = user.Username,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Email = user.Email,
                    Phone = user.Phone == null ? 0 : user.Phone.PhoneNumber,
                    Picture = user.Picture.ImagePath,
                    Role = user.Role.ToString(),
                    Posts = (ICollection<PostResponseDto>)new PostMapper().ConvertToResponse(user.Posts),
                    Comments = (ICollection<CommentResponseDto>)new CommentMapper().ConvertToResponse(user.Comments)
                };                
            }
            return response;
        }
    }
}