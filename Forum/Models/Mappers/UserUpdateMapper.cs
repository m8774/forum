﻿using Forum.Enums;
using Forum.Models.Dtos;
using Forum.Models.Views;
using System;

namespace Forum.Models.Mappers
{
    public class UserUpdateMapper
    {
		public User ConvertToModel(UserUpdateDto dto)
		{
			User model = new User();
			model.FirstName = dto.FirstName;
            model.LastName = dto.LastName;
            model.Email = dto.Email;
            model.Password = dto.Password;
            model.Role = dto.Role;
            model.Phone = new Phone() { PhoneNumber = dto.Phone };
            model.Picture = new ProfilePic() { ImagePath = dto.Picture };

            return model;
		}

        internal UserUpdateDto ConvertToDto(UserResponseDto user)
        {
            UserUpdateDto model = new UserUpdateDto();
            Enum.TryParse(user.Role, out Roles parsedRole);

            model.FirstName = user.FirstName;
            model.LastName = user.LastName;
            model.Email = user.Email;
            model.Role = parsedRole;
            model.Phone = user.Phone;
            model.Picture = user.Picture;

            return model;
        }
    }
}