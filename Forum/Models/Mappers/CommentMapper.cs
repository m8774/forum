﻿using Forum.Models.Dtos;
using System.Collections.Generic;
using System.Linq;

namespace Forum.Models.Mappers
{
    public class CommentMapper
    {
        public IEnumerable<CommentResponseDto> ConvertToResponse(IEnumerable<Comment> comments)
        {
            CommentResponseDto[] response = new CommentResponseDto[comments.Count()];
            foreach (var comment in comments)
            {
                response[comments.ToList().IndexOf(comment)] = new CommentResponseDto
                {
                    CommentId = comment.Id,
                    PostId = comment.PostId,
                    AuthorId = comment.AuthorId,
                    Title = comment.Title,
                    Content = comment.Content,
                    AuthorUsername = comment.Author.Username,
                    CreatedOn = comment.DateCreated.ToString()
                };
            }
            return response;
        }
    }
}
