using Forum.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Forum.Models
{
    public class User : Entity
    {
        [Required(ErrorMessage = "* Please enter your first name.")]
        [StringLength(Restrictions.NameMaxLen, MinimumLength = Restrictions.NameMinLen, ErrorMessage = "* The {0} numbers must be between {2} and {1} character in length.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "* Please enter your last name.")]
        [StringLength(Restrictions.NameMaxLen, MinimumLength = Restrictions.NameMinLen, ErrorMessage = "* The {0} numbers must be between {2} and {1} character in length.")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "* Please enter your username.")]
        public string Username { get; set; }
        
        [Required(ErrorMessage = "* Please enter your password.")]
        public string Password { get; set; }
        
        [Required(ErrorMessage = "* The email address is required")]
        [EmailAddress(ErrorMessage = "* Invalid Email Address")]
        public string Email { get; set; }
        public Roles Role { get; set; }
        public Phone? Phone { get; set; }
        public ProfilePic Picture { get; set; }
        public int PictureId { get; set; }
        public ICollection<Post> Posts { get; set; } = new List<Post>();
        public ICollection<Comment> Comments { get; set; } = new List<Comment>();
        public ICollection<Post> UpVotes { get; set; } = new List<Post>();
        public ICollection<Post> DownVotes { get; set; } = new List<Post>();
        public override bool Equals(object obj)
        {
            if (obj is User other)
            {
                return this.Username == other.Username;
            }

            return false;
        }

        public override int GetHashCode()
        {
            return this.Username.GetHashCode();
        }

    }
}
