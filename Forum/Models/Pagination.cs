﻿namespace Forum.Models
{
    public class Pagination : IPagination
    {
        public Pagination(int totalPages, int pageNumber, bool hasNextPage, bool hasPreviosPage)
        {
            TotalPages = totalPages;
            PageNumber = pageNumber;
            HasNextPage = hasNextPage;
            HasPreviosPage = hasPreviosPage;
        }

        public bool HasPreviosPage { get; }

        public bool HasNextPage { get; }
        public int TotalPages { get; }
        public int PageNumber { get; }
    }
}