﻿using System.ComponentModel.DataAnnotations;

namespace Forum.Models
{
    public class Threads:Entity
    {
        [Required(ErrorMessage = "* Please enter your title.")]
        [StringLength(Restrictions.TitleMaxLen, MinimumLength = Restrictions.TitleMinLen, ErrorMessage = "* The {0} title must be between {2} and {1} character in length.")]
        public string Title { get; set; }

        [Required(ErrorMessage = "* Please enter your content.")]
        [StringLength(Restrictions.ContentMaxLen, MinimumLength = Restrictions.ContentMinLen, ErrorMessage = "* The {0} content must be between {2} and {1} character in length.")]
        public string Content { get; set; }
        public int AuthorId { get; set; }
        public User Author { get; set; }
    }
}
