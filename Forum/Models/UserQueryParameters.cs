﻿namespace Forum.Models
{
    public class UserQueryParameters
    {
        const int maxPageSize = 20;
        public int PageNumber { get; set; } = 1;

        private int pageSize = 10;
        public int PageSize
        {
            get
            {
                return pageSize;
            }
            set
            {
                pageSize = (value > maxPageSize) ? maxPageSize : value;
            }
        }
        public string SortBy { get; set; } = "";
        public string FilterByUsername { get; set; } = "";
        public string FilterByEmail { get; set; } = "";
        public string FilterByFirstName { get; set; } = "";
        public string OrderBy { get; set; } = "";
        public string SearchQuery { get; set; } = "";
    }
}