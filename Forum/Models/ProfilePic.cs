﻿
using System.Collections.Generic;

namespace Forum.Models
{
    public class ProfilePic: Entity
    {
        public string ImageName { get; set; }
        public string ImagePath { get; set; }

        public User User;
        public int UserId;
      
    }
}
