﻿namespace Forum.Models
{
    public class PostQueryParameters
    {
        public const int MaxPageSize = 20;
        public int PageNumber { get; set; } = 1;
        private int pageSize = 10;
        public int PageSize
        {
            get
            {
                return pageSize;
            }
            set
            {
                pageSize = (value > MaxPageSize) ? MaxPageSize : value;
            }
        }
        public string SortBy { get; set; } = "";
        public string FilterByUsername { get; set; } = "";
        public string FilterByTags { get; set; } = "";
        public string SearchQuery { get; set; } = "";
        public string OrderBy { get; set; } = "";
    }
}