﻿using System.Collections.Generic;

namespace Forum.Models
{
    public class Tag:Entity
    {
        public string Name { get; set; }
        public IEnumerable<Post> Posts { get; set; } = new List<Post>();
    }
}
