﻿namespace Forum.Models
{
    public interface IPagination
    {
        bool HasNextPage { get; }
        bool HasPreviosPage { get; }
        int PageNumber { get; }
        int TotalPages { get; }
    }
}