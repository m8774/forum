﻿using System.Collections.Generic;

namespace Forum.Models
{
    public class WelcomeScreen
    {
        public WelcomeScreen()
        {
            WelcomeMessage = "Hello and welcome to our Forum!";
            PostsCount = 0;
            UsersCount = 0;
            LogIn = "You're not logged in! To log in, please enter USERNAME and PASSWORD as header parameters.";
            
            ICollection<KeyValuePair<string, string>> commands = new Dictionary<string, string>();
            
            commands.Add(new KeyValuePair<string, string>("Register", "POST api/users/"));
            commands.Add(new KeyValuePair<string, string>("Top 10 commented posts", "GET api/posts?PageNumber=1&PageSize=10&SortBy=comments&OrderBy=descending"));
            commands.Add(new KeyValuePair<string, string>("Top 10 newest posts", "GET api/posts?PageNumber=1&PageSize=10&SortBy=date&OrderBy=descending"));

            Commands = commands;

        }

        public string WelcomeMessage { get; set; }
        public int PostsCount { get; set; }
        public int UsersCount { get; set; }
        public string LogIn { get; set; }
        public ICollection<KeyValuePair<string, string>> Commands {get; set;}    
    }
}
