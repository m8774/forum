﻿namespace Forum.Models
{
    public class Phone:Entity
    {
        public long? PhoneNumber { get; set; } = 0;
        public int UserId { get; set; }
        public User User { get; set; }
    }
}
