﻿namespace Forum.Models.Dtos
{
    public class TagDto
    {
        public string Tag { get; set; }
    }
}
