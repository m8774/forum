﻿using System;
using System.Collections.Generic;

namespace Forum.Models.Dtos
{
    public class PostResponseDto
    {
        public string CreatedOn { get; set; }
        public string ModifiedOn { get; set; }
        public int PostId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public int AuthorId { get; set; }
        public string AuthorUsername { get; set; }
        public int UpVotes { get; set; }
        public int DownVotes { get; set; }

        public ICollection<CommentResponseDto> Comments { get; set; }
        public ICollection<TagDto> Tags { get; set; }
    }
}
