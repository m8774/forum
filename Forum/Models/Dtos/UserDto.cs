﻿using System.ComponentModel.DataAnnotations;

namespace Forum.Models.Dtos
{
    public class UserDto
    {
        [StringLength(Restrictions.NameMaxLen, MinimumLength = Restrictions.NameMinLen, ErrorMessage = "* The {0} numbers must be between {2} and {1} character in length.")]
        public string FirstName { get; set; }

        [StringLength(Restrictions.NameMaxLen, MinimumLength = Restrictions.NameMinLen, ErrorMessage = "* The {0} numbers must be between {2} and {1} character in length.")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "* Please enter your username.")]
        public string Username { get; set; }

        [Required(ErrorMessage = "* Please enter your password.")]
        public string Password { get; set; }

        [Required(ErrorMessage = "* The email address is required")]
        [EmailAddress(ErrorMessage = "* Invalid Email Address")]
        public string Email { get; set; }

    }
}