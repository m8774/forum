﻿namespace Forum.Models.Dtos
{
	public class PostViewDto : PostDto
	{
        public new string Tags { get; set; }
    }
}
