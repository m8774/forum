﻿using Forum.Enums;
using System.ComponentModel.DataAnnotations;

namespace Forum.Models.Dtos
{
    public class UserUpdateDto
    {
        [StringLength(Restrictions.NameMaxLen, MinimumLength = Restrictions.NameMinLen, ErrorMessage = "* The {0} numbers must be between {2} and {1} character in length.")]
        public string FirstName { get; set; }

        [StringLength(Restrictions.NameMaxLen, MinimumLength = Restrictions.NameMinLen, ErrorMessage = "* The {0} numbers must be between {2} and {1} character in length.")]
        public string LastName { get; set; }
        public string Password { get; set; }

        [EmailAddress(ErrorMessage = "* Invalid Email Address")]
        public string Email { get; set; }
        public long? Phone { get; set; }
        public string Picture { get; set; }
        public Roles Role { get; set; }
    }
}