﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Forum.Models.Dtos
{
    public class PostDto
    {
        [Required]
        [StringLength(Restrictions.TitleMaxLen, MinimumLength = Restrictions.TitleMinLen, ErrorMessage = "* The {0} title must be between {2} and {1} character in length.")]
        public string Title { get; set; }

        [Required]
        [StringLength(Restrictions.ContentMaxLen, MinimumLength = Restrictions.ContentMinLen, ErrorMessage = "* The {0} content must be between {2} and {1} character in length.")]
        public string Content { get; set; }
        public ICollection<TagDto> Tags { get; set; }

    }
}