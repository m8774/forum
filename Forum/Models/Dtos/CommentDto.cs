﻿using System.ComponentModel.DataAnnotations;

namespace Forum.Models.Dtos
{
    public class CommentDto
    {
        [StringLength(Restrictions.ContentMaxLen, MinimumLength = Restrictions.ContentMinLen, ErrorMessage = "* The {0} content must be between {2} and {1} character in length.")]
        public string Content { get; set; }
    }
}