﻿using System;

namespace Forum.Models.Dtos
{
    public class CommentResponseDto
    {
        public string CreatedOn { get; set; }
        public int CommentId { get; set; }
        public int PostId { get; set; }
        public string Title { get; set; }
        public string Content { get; set; }
        public int AuthorId { get; set; }
        public string AuthorUsername { get; set; }
    }
}
