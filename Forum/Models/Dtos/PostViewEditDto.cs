﻿namespace Forum.Models.Dtos
{
    public class PostViewEditDto : PostResponseDto
    {
        public new string Tags { get; set; }
    }
}
