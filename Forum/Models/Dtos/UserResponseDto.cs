﻿using System;
using System.Collections.Generic;

namespace Forum.Models.Dtos
{
    public class UserResponseDto
    {
        public int UserId { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public long? Phone { get; set; }
        public string Picture { get; set; }
        public string Role { get; set; }
        public string RegisteredOn { get; set; }
        public ICollection<PostResponseDto> Posts { get; set; }
        public ICollection<CommentResponseDto> Comments { get; set; }
    }
}
