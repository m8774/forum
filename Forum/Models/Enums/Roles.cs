
namespace Forum.Enums
{
    public enum Roles
    {
        Null,
        Admin,
        Moderator,
        User,
        Blocked,
        Banned,
    }
}