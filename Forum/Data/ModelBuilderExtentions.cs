﻿using Forum.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;

namespace Forum.Data
{
    public static class ModelBuilderExtentions
    {
        private static Uri DefaultImage = new Uri("https://www.kindpng.com/picc/m/126-1267446_assassins-creed-pixel-art-hd-png-download.png");
        public static void Seed(this ModelBuilder modelBuilder)
        {



            // Seed the database with users
            List<User> users = GetUsers();
            modelBuilder.Entity<User>().HasData(users);

            // Seed the database with posts
            List<Post> posts = GetPosts();
            modelBuilder.Entity<Post>().HasData(posts);

            // Seed the database with comments
            List<Comment> comments = GetComments();
            modelBuilder.Entity<Comment>().HasData(comments);

            // Seed the datavase with pictures
            List<ProfilePic> pictures = GetPictures();
            modelBuilder.Entity<ProfilePic>().HasData(pictures);

            // Seed the database with phones
            List<Phone> phones = GetPhones();
            modelBuilder.Entity<Phone>().HasData(phones);


        }

        public static List<Phone> GetPhones()
        {
            var phones = new List<Phone>();
            phones.Add(new Phone
            {
                Id = 1,
                UserId = 1,
                PhoneNumber = 99988877,
                DateCreated = DateTime.Now,
                DateModified = DateTime.Now
            });

            return phones;

        }
        public static List<ProfilePic> GetPictures()
        {
            var pictures = new List<ProfilePic>();

            byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(DefaultImage.ToString());


            pictures.Add(new ProfilePic
            {
                Id = 1,
                UserId = 1,
                ImageName = "Admin",
                ImagePath = System.Convert.ToBase64String(toEncodeAsBytes),
                DateCreated = DateTime.Now,
                DateModified = DateTime.Now

            });

            pictures.Add(new ProfilePic
            {
                Id = 2,
                UserId = 2,
                ImageName = "Elon",
                ImagePath = System.Convert.ToBase64String(toEncodeAsBytes),
                DateCreated = DateTime.Now,
                DateModified = DateTime.Now

            });

            return pictures;
        }

        public static List<Comment> GetComments()
        {
            var comments = new List<Comment>();
            comments.Add(new Comment
            {
                Id = 1,
                PostId = 1,
                Content = "Free Speach fore everyone! Unblick Donald Trump.",
                Title = "Seeded comment for Twitter post from Elon Musk.",
                AuthorId = 1
            });

            comments.Add(new Comment
            {
                Id = 2,
                PostId = 1,
                Content = "Yeeeesssss this is what I want! Bring back the meme lord",
                Title = "Seeded comment for Twitter post from Elon Musk.",
                AuthorId = 2
            });
            return comments;
        }
        public static List<Post> GetPosts()
        {
            var posts = new List<Post>();
           posts.Add(new Post
           {
               Id = 1,
               Title = "This is a post regarding Twitter",
               Content = "Today is a great day! I bought Twitter!",
               AuthorId = 2,
               UpVotes = 0,
               DownVotes = 0


            });
            return posts;
        }

        public static List<User> GetUsers()
        {
            var users = new List<User>();
            users.Add(new User
            {
                Id = 1,
                FirstName = "I am Admin",
                LastName = "Adminov",
                Username = "admin",
                Password = "admin",
                Email = "admin@localhost.com",
                Role = Enums.Roles.Admin,
                PictureId = 1

            });
            users.Add(new User
            {
                Id = 2,
                FirstName = "Elon",
                LastName = "Musk",
                Username = "user1",
                Password = "user1",
                Email = "elon@localhost.com",
                Role = Enums.Roles.User,
                PictureId = 2


            });
            return users;
        }
    }
}
