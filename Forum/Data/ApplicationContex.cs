﻿using Forum.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace Forum.Data
{
    public class ApplicationContext : DbContext
    {
        public ApplicationContext(DbContextOptions<ApplicationContext> options)
            : base(options)
        {
        }

        public DbSet<Post> Posts { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Phone> Phones { get; set; }
        public DbSet<ProfilePic> Pictures { get; set; }

        public override int SaveChanges()
        {
            //DeletionHelper();
            return base.SaveChanges();
        }

        private void DeletionHelper()
        {
            this.ChangeTracker.DetectChanges();
            var entities = ChangeTracker.Entries()
                                .Where(e => e.State == EntityState.Deleted);

            foreach (var e in entities)
            {
                if (e.Entity is User)
                {
                    e.State = EntityState.Modified;
                    var user = e.Entity as User;
                    user.IsDeleted = true;
                    user.DateModified = System.DateTime.Now;
                }
            }
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Phone>()
                .HasOne(x => x.User)
                .WithOne(x => x.Phone)
                .HasForeignKey<Phone>(x => x.UserId);
                //.OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Comment>()
                .HasOne(x => x.Author)
                .WithMany(x => x.Comments)
                .HasForeignKey(x => x.AuthorId);
                //.OnDelete(DeleteBehavior.NoAction);

            modelBuilder.Entity<Post>()
                .HasOne(x=>x.Author)
                .WithMany(x=>x.Posts)
                .HasForeignKey(x=>x.AuthorId);


            modelBuilder.Entity<ProfilePic>()
                .HasOne(x => x.User)
                .WithOne(x => x.Picture)
                .HasForeignKey<ProfilePic>(x => x.UserId);


            modelBuilder.Entity<User>()
                .HasQueryFilter(u => !u.IsDeleted);

            base.OnModelCreating(modelBuilder);
            modelBuilder.Seed();
        }
    }
}