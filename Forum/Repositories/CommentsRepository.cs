﻿using Forum.Data;
using Forum.Exceptions;
using Forum.Models;
using Forum.Repositories.Helpers;
using Forum.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;

namespace Forum.Repositories
{
    public class CommentsRepository : ICommentsRepository
    {
        public readonly ApplicationContext context;

        public CommentsRepository(ApplicationContext context)
        {
            this.context = context;
        }
        public Comment Create(Comment comment)
        {
            comment.DateCreated = comment.DateModified = System.DateTime.Now;

            var c = this.context.Comments.Add(comment);
            this.context.SaveChanges();

            return c.Entity;
        }

        public Comment Delete(int id)
        {
            var commentToDelete = this.Get(id);
            var c = this.context.Remove(commentToDelete);
            this.context.SaveChanges();

            return c.Entity;
        }

        public IQueryable<Comment> Get()
        {
            return this.context.Comments;
        }

        public Comment Get(int id)
        {
            var c = this.context.Comments.Where(comment => comment.Id == id).Include(a => a.Author).Include(p => p.Post).FirstOrDefault();

            return c ?? throw new EntityNotFoundException();
        }

        public Comment Get(User author)
        {
            var c = this.context.Comments.Where(comment => comment.AuthorId == author.Id).Include(a => a.Author).Include(p => p.Post).FirstOrDefault();

            return c ?? throw new EntityNotFoundException();
        }

        public IQueryable<Comment> Get(CommentQueryParameters filterParameters)
        {
            string username = !string.IsNullOrEmpty(filterParameters.FilterByUsername) ? filterParameters.FilterByUsername.ToLowerInvariant() : string.Empty;
            string searchQuery = !string.IsNullOrEmpty(filterParameters.SearchQuery) ? filterParameters.SearchQuery.ToLowerInvariant() : string.Empty;
            string sortBy = !string.IsNullOrEmpty(filterParameters.SortBy) ? filterParameters.SortBy.ToLowerInvariant() : string.Empty;
            string orderBy = !string.IsNullOrEmpty(filterParameters.OrderBy) ? filterParameters.OrderBy.ToLowerInvariant() : string.Empty;

            var result = FilterThreads<Comment>.SearchByAuthor(username, this.context.Comments);
            result = FilterThreads<Comment>.SearchByKeyWords(searchQuery, result).Include(a => a.Author).Include(p => p.Post);

            return FilterThreads<Comment>.OrderBy(sortBy, orderBy, result);

          

        }

        public Comment Update(int id, Comment comment)
        {
            var commentToUpdate = this.Get(id);
            commentToUpdate.Content = comment.Content;
            commentToUpdate.DateModified = System.DateTime.Now;
            this.context.SaveChanges();

            return this.Get(id);
        }
    }
}
