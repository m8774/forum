﻿using Forum.Models;

namespace Forum.Repositories.Interfaces
{
    public interface IPhoneRepository
    {
        public Phone Get(int id);
        public Phone Create(long? number, User user);
        public Phone Update(int id, long number);
        public Phone Delete(int id);
    }
}
