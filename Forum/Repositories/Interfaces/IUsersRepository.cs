﻿using Forum.Models;
using System.Collections.Generic;
using System.Linq;

namespace Forum.Repositories.Interfaces
{
    public interface IUsersRepository
    {
        IQueryable<User> Get();
        User Get(int id);
        User Get(string username);
        IQueryable<User> Get(UserQueryParameters filterParameters);
        User Create(User user);
        User Update(User user);
        User Delete(int id);
        User Get(int id, UserQueryParameters filterParameters);
        int Count();
    }
}