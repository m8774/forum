﻿using Forum.Models;
using System.Collections.Generic;
using System.Linq;

namespace Forum.Repositories.Interfaces
{
    public interface IPostsRepository
	{
		IQueryable<Post> Get();
		Post Get(int id);
		Post Get(User author);
		IQueryable<Post> Get(PostQueryParameters filterParameters);
		Post Create(Post post);
		Post Update(int id, Post post);
		Post Delete(int id);
		Post UpVote(int id);
		Post DownVote(int id);
        int Count();
    }
}