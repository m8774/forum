﻿using Forum.Models;

namespace Forum.Repositories.Interfaces
{
    public interface IImageRepository
    {
        public ProfilePic Create(User user, string src = "default");
        public ProfilePic Get(int id);
        public ProfilePic Get(string src);
        public ProfilePic Update(int id, string src, User user);
        public ProfilePic Delete(int id);
    }
}
