﻿using Forum.Models;
using System.Collections.Generic;
using System.Linq;

namespace Forum.Repositories.Interfaces
{
    public interface ICommentsRepository
	{
		IQueryable<Comment> Get();
		Comment Get(int id);
		Comment Get(User author);
		IQueryable<Comment> Get(CommentQueryParameters filterParameters);
		Comment Create(Comment comment);
		Comment Update(int id, Comment comment);
		Comment Delete(int id);
	}
}