﻿using Forum.Models;

namespace Forum.Repositories.Interfaces
{
    public interface ITagsRepository
    {
        public Tag Create(string newTag);
        public Tag Update(string tag, Post p);
        public Tag Delete();
        public Tag Get(string tagName);

    }
}
