﻿using Forum.Exceptions;
using Microsoft.EntityFrameworkCore;
using Forum.Models;
using System.Linq;
using System.Reflection;
using System.Collections.Generic;
using System;
using System.ComponentModel;
using System.Linq.Expressions;

namespace Forum.Repositories.Helpers
{
    public static class FilterThreads<T> where T: Threads
    {
        public static IQueryable<T> SearchByAuthor(string username, DbSet<T> entity) 
        {

            if (entity is null)
            {
                throw new ResourceUnvailableException(nameof(entity));
            }
            if (username == string.Empty)
            {
                return entity;
            }

            return entity.Where(u => u.Author.Username == username);
        }


        public static IQueryable<T> SearchByKeyWords(string query, IQueryable<T> entity)
        {

            if (entity is null)
            {
                throw new ResourceUnvailableException(nameof(entity));
            }
            if (query == string.Empty)
            {
                return entity;
            }

            
            return entity.Where(c => c.Content.Contains(query) || c.Title.Contains(query));
        }

        public static IQueryable<T> OrderBy(string property, string order, IQueryable<T> entity)
        {


            if(property == "created" || string.IsNullOrEmpty(property))
            {
                property = "DateCreated";
            }
            else if (property == "modified")
            {
                property = "DateModified";
            }
            else
            {
                property = string.Empty;
            }

            if (string.IsNullOrEmpty(property))
            {
                return entity;
            }

            var sorted = entity;

            if (property == "comments")
            {
                if (order == "descending")
                {

                    return sorted.OrderByDescending(p => (p as Post).Comments.Count);
                }

                else
                {
                    return sorted.OrderBy(p => ( p as Post ).Comments.Count);
                }

            }


            if(order == "descending")
            {
                
                return sorted.OrderByDescending(ToLambda<T>(property));
            }

            else
            {
                return sorted.OrderBy(ToLambda<T>(property));
            }

        }

        private static Expression<Func<T, object>> ToLambda<T>(string propertyName)
        {
            var parameter = Expression.Parameter(typeof(T));
            var property = Expression.Property(parameter, propertyName);
            var propAsObject = Expression.Convert(property, typeof(object));

            return Expression.Lambda<Func<T, object>>(propAsObject, parameter);
        }

        internal static IQueryable<Post> SearchByTags(string tag, DbSet<Post> entity)
        {
            if (entity is null)
            {
                throw new ResourceUnvailableException(nameof(entity));
            }
            if (tag == string.Empty)
            {
                return entity;
            }
            
            return entity.Where(p => p.Tags.Where(t => t.Name.Equals(tag)).Any());
        }
    }
}
