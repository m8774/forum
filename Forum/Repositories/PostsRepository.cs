﻿using Forum.Models;
using Forum.Repositories.Interfaces;
using System.Collections.Generic;
using Forum.Data;
using System.Linq;
using Forum.Exceptions;
using System;
using Microsoft.EntityFrameworkCore;
using Forum.Repositories.Helpers;
using System.Collections;

namespace Forum.Repositories
{
    public class PostsRepository : IPostsRepository
    {
        public readonly ApplicationContext context;
        public PostsRepository(ApplicationContext context)
        {
            this.context = context;
        }


        public Post Create(Post post)
        {
            post.DateCreated = post.DateModified = DateTime.Now;
            var p = this.context.Posts.Add(post).Entity;
            this.context.SaveChanges();

            return p;
        }

        public Post Delete(int id)
        {
            var postToDelete = this.Get(id);
            
            if(postToDelete.Tags.Count > 0)
            {
               
                foreach (var t in postToDelete.Tags)
                {
                    t.Posts.ToList().Remove(postToDelete);
                }
            }
            var p = this.context.Remove(postToDelete).Entity;
            this.context.SaveChanges();

            return p;

        }

        public IQueryable<Post> Get()
        {
            return this.context.Posts.OrderBy(p => p.Id)
                        .Include(c => c.Comments)
                        .Include(t => t.Tags)
                        .Include(a => a.Author);
        }

        public Post Get(int id)
        {
            var p = this.context.Posts.Where(post => post.Id == id)
                                        .Include(c => c.Comments)
                                            .ThenInclude(c => c.Author)
                                        .Include(t => t.Tags)
                                        .Include(a => a.Author)
                                        .FirstOrDefault();

            return p ?? throw new EntityNotFoundException();
        }

        public Post Get(User author)
        {
            var p = this.context.Posts.Where(post => post.AuthorId == author.Id)
                                        .Include(c => c.Comments)
                                        .Include(t => t.Tags)
                                        .Include(a => a.Author)
                                        .FirstOrDefault();

            return p ?? throw new EntityNotFoundException();
        }

        public IQueryable<Post> Get(PostQueryParameters filterParameters)
        {
            string username = !string.IsNullOrEmpty(filterParameters.FilterByUsername) ? filterParameters.FilterByUsername.ToLowerInvariant() : string.Empty;
            string searchQuery = !string.IsNullOrEmpty(filterParameters.SearchQuery) ? filterParameters.SearchQuery.ToLowerInvariant() : string.Empty;
            string sortBy = !string.IsNullOrEmpty(filterParameters.SortBy) ? filterParameters.SortBy.ToLowerInvariant() : string.Empty;
            string orderBy = !string.IsNullOrEmpty(filterParameters.OrderBy) ? filterParameters.OrderBy.ToLowerInvariant() : string.Empty;
            string tag = !string.IsNullOrEmpty(filterParameters.FilterByTags) ? filterParameters.FilterByTags.ToLowerInvariant() : string.Empty;

            var result = FilterThreads<Post>.SearchByAuthor(username, this.context.Posts);
            result = FilterThreads<Post>.SearchByTags(tag, this.context.Posts);
            result = FilterThreads<Post>.SearchByKeyWords(searchQuery, result).Include(c => c.Comments).ThenInclude(ca => ca.Author)
                                                                              .Include(t => t.Tags)
                                                                              .Include(a => a.Author);


            
            return FilterThreads<Post>.OrderBy(sortBy, orderBy, result);

          


        }

        public Post Update(int id, Post post)
        {
            var postToUpdate = this.Get(id);

            if (!string.IsNullOrEmpty(post.Title))
            {
                postToUpdate.Title = post.Title;
            }

            if (!string.IsNullOrEmpty(post.Content))
            {
                postToUpdate.Content = post.Content;
            }

            if (post.Tags.Count > 0)
            {
                postToUpdate.Tags.Clear();
                postToUpdate.Tags.ToList().AddRange(post.Tags);
                

            }
            if (!string.IsNullOrEmpty(post.Content) || !string.IsNullOrEmpty(post.Title) || post.Tags.Count > 0)
            {
                postToUpdate.DateModified = DateTime.Now;

                this.context.Update(postToUpdate);
                this.context.SaveChanges();
            }

            return this.Get(id);
        }

        public Post UpVote(int id)
        {
            var postToUpvote = this.Get(id);
            postToUpvote.UpVotes++;
            this.context.Update(postToUpvote);
            this.context.SaveChanges();

            return postToUpvote;
        }
        public Post DownVote(int id)
        {
            var postToDownvote = this.Get(id);
            postToDownvote.DownVotes++;
            this.context.Update(postToDownvote);
            this.context.SaveChanges();

            return postToDownvote;
        }

        public int Count()
        {
            return this.context.Posts.Count();
        }
    }
}
