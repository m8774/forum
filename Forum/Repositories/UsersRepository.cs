﻿using Forum.Data;
using Forum.Models;
using Forum.Repositories.Interfaces;
using System.Collections.Generic;
using System.Linq;
using Forum.Exceptions;
using Microsoft.EntityFrameworkCore;

namespace Forum.Repositories
{
    public class UsersRepository : IUsersRepository
    {
        public readonly ApplicationContext context;

        public UsersRepository(ApplicationContext context)
        {
            this.context = context;
        }

        public int Count()
        {
            return this.context.Users.Count();
        }

        public User Create(User user)
        {
            user.DateCreated = user.DateModified = System.DateTime.Now;
            var u = this.context.Users.Add(user);
            this.context.SaveChanges();
            return u.Entity;
        }

        public User Delete(int id)
        {
            var userToDelete = this.Get(id);
            var u = this.context.Remove(userToDelete).Entity;
            this.context.SaveChanges();

            return u;
        }


        public User Get(int id)
        {
            var u = this.context.Users
                .Where(user => user.Id == id)
                .Include(p => p.Posts)
                .Include(c => c.Comments)
                .Include(x => x.Phone)
                .Include(u => u.Picture)
                .Include(x => x.UpVotes)
                .Include(x => x.DownVotes)
                .FirstOrDefault();

            return u;
        }

        public User Get(string username)
        {

            var u = this.context.Users.Where(user => user.Username == username)
                .Include(p => p.Posts)
                .Include(c => c.Comments)
                .Include(x => x.Phone)
                .Include(u => u.Picture)
                .Include(x => x.UpVotes)
                .Include(x => x.DownVotes)
                .FirstOrDefault();

            return u;
        }

        public User Get(int id, UserQueryParameters filterParameters)
        {
            var u = this.context.Users.Where(user => user.Id == id)
                .Include(p => p.Posts)
                .Include(c => c.Comments)
                .Include(x => x.Phone)
                .Include(u => u.Picture)
                .Include(x => x.UpVotes)
                .Include(x => x.DownVotes)
                .FirstOrDefault();

            if (u == null)
            {
                throw new EntityNotFoundException();
            }

            return u;
        }

        public IQueryable<User> Get(UserQueryParameters filterParameters)
        {
            string username = !string.IsNullOrEmpty(filterParameters.FilterByUsername) ? filterParameters.FilterByUsername.ToLowerInvariant() : string.Empty;
            string email = !string.IsNullOrEmpty(filterParameters.FilterByEmail) ? filterParameters.FilterByEmail.ToLowerInvariant() : string.Empty;
            string firstname = !string.IsNullOrEmpty(filterParameters.FilterByFirstName) ? filterParameters.FilterByFirstName.ToLowerInvariant() : string.Empty;

            string sortBy = !string.IsNullOrEmpty(filterParameters.SortBy) ? filterParameters.SortBy.ToLowerInvariant() : string.Empty;
            string orderBy = !string.IsNullOrEmpty(filterParameters.OrderBy) ? filterParameters.OrderBy.ToLowerInvariant() : string.Empty;

            var users = this.context.Users
                .Where(u =>
                u.Username.Contains(username)
                &&
                u.Email.Contains(email)
                &&
                u.FirstName.Contains(firstname)
                )
                .Include(p => p.Posts)
                .Include(c => c.Comments)
                .Include(x => x.Phone)
                .Include(u => u.Picture)
                .Include(x => x.UpVotes)
                .Include(x => x.DownVotes);

            if (sortBy == "registered" || sortBy == string.Empty)
            {
                return orderBy == "descending" ? users.OrderByDescending(u => u.DateCreated) : users.OrderBy(u => u.DateCreated);
            }
            else
            {
                return orderBy == "descending" ? users.OrderByDescending(u => u.Username) : users.OrderBy(u => u.Username);
            }
        }

        public IQueryable<User> Get()
        {
            return this.context.Users
                .Include(p => p.Posts)
                .Include(c => c.Comments)
                .Include(x => x.Phone)
                .Include(u => u.Picture)
                .Include(x => x.UpVotes)
                .Include(x => x.DownVotes);

        }

        public User Update(User user)
        {
            var userToUpdate = this.Get(user.Id);

            if (userToUpdate == null)
            {
                throw new EntityNotFoundException();
            }

            userToUpdate = user;

            this.context.Update(userToUpdate);
            this.context.SaveChanges();

            return this.Get(user.Id);
        }
    }
}
