﻿using Forum.Data;
using Forum.Models;
using Forum.Repositories.Interfaces;

namespace Forum.Repositories
{
    public class PhoneRepository:IPhoneRepository
    {
        private readonly ApplicationContext context;

        public PhoneRepository(ApplicationContext context)
        {
            this.context = context;
        }

        public Phone Create(long? number, User user)
        {
            Phone p = new Phone
            {
                PhoneNumber = number,
                DateCreated = System.DateTime.Now,
                DateModified = System.DateTime.Now,
                User = user,
                UserId = user.Id

            };

            return p;
        }

        public Phone Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public Phone Get(int id)
        {
            throw new System.NotImplementedException();
        }

        public Phone Update(int id, long number)
        {
            throw new System.NotImplementedException();
        }
    }
}
