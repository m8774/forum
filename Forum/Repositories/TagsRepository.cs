﻿using Forum.Data;
using Forum.Exceptions;
using Forum.Models;
using Forum.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Forum.Repositories
{
    public class TagsRepository : ITagsRepository
    {
        private readonly ApplicationContext context;

        public TagsRepository(ApplicationContext context)
        {
            this.context = context;
        }

        public Tag Create(string newTag)
        {
            var tag = this.Get(newTag);

            if (tag != null)
            {
                return tag;
            }

            tag = new Tag()
            {
                Name = newTag,
                DateCreated = DateTime.Now,
                DateModified = DateTime.Now

            };

            this.context.Add(tag);
            this.context.SaveChanges();

            return this.Get(newTag);
        }

        public Tag Delete()
        {
            throw new System.NotImplementedException();
        }

        public Tag Get(string tagName)
        {
            return this.context.Tags.Where(t => t.Name == tagName).FirstOrDefault();
        }

        public Tag Update(string tag, Post p)
        {
            var t = this.Get(tag);

            if (t.Posts == null)
            {
                t.Posts = new List<Post>();
            }
            t.Posts.ToHashSet().Add(p);
            this.context.Update(t);
            this.context.SaveChanges();

            return t;
        }
    }
}
