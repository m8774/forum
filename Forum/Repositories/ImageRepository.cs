﻿using Forum.Data;
using Forum.Models;
using Forum.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Forum.Repositories
{
    public class ImageRepository : IImageRepository
    {
        private readonly ApplicationContext context;
        private Uri defaultImage = new Uri("https://www.kindpng.com/picc/m/126-1267446_assassins-creed-pixel-art-hd-png-download.png");

        public ImageRepository(ApplicationContext context)
        {
            this.context = context;
        }
        public ProfilePic Create(User user, string src = "default")
        {
            if (src.Equals("default"))
            {
                src = defaultImage.ToString();
            }
            ProfilePic pic = new ProfilePic();
            pic.User = user;
            pic.UserId = user.Id;
            pic.ImageName = user.Username;
            byte[] toEncodeAsBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(src);
            pic.ImagePath = System.Convert.ToBase64String(toEncodeAsBytes);

            var newPicture = this.context.Pictures.Add(pic);
            this.context.SaveChanges();

            return newPicture.Entity;


        }

        public ProfilePic Get(string src)
        {
            return this.context.Pictures.Where(p => p.ImagePath == src).FirstOrDefault();
        }

        public ProfilePic Delete(int id)
        {
            throw new System.NotImplementedException();
        }

        public ProfilePic Get(int id)
        {
            return this.context.Pictures.Where(p => p.Id == id).FirstOrDefault();
        }

        public ProfilePic Update(int id, string src, User user)
        {
            throw new System.NotImplementedException();
        }
    }
}
