﻿using Forum.Controllers.Helpers;
using Forum.Enums;
using Forum.Exceptions;
using Forum.Models;
using Forum.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace Forum.Controllers.Api
{
    [Route("api")]
    [ApiController]
    public class HomeController : ControllerBase
    {
        private readonly IPostsService postsService;
        private readonly IUsersService usersService;
        private readonly AuthenticationHelper authenticationHelper;

        private readonly ILogger<PostsController> logger;

        public HomeController(ILogger<PostsController> logger, IPostsService postsService, IUsersService usersService, AuthenticationHelper authenticationHelper)
        {
            this.logger = logger;
            this.postsService = postsService;
            this.usersService = usersService;
            this.authenticationHelper = authenticationHelper;
        }

        // GET: api
        [HttpGet("")]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        public IActionResult Get([FromHeader] string username, [FromHeader] string password)
        {
            try
            {
                int postsCount = postsService.GetCount();
                int usersCount = usersService.GetCount();

                WelcomeScreen welcomeMessage = new WelcomeScreen();
                welcomeMessage.PostsCount = postsCount;
                welcomeMessage.UsersCount = usersCount;

                var user = authenticationHelper.TryGetUser(username, password);

                if (user == null)
                {
                    logger.LogInformation("Anonymous user visited the site.");
                    return StatusCode(StatusCodes.Status200OK, welcomeMessage);
                }

                logger.LogInformation("User {0} visited the site", user.Username);
                welcomeMessage.LogIn = "Logged in as " + user.Username;

                ICollection<KeyValuePair<string, string>> commands = new Dictionary<string, string>();

                //check if user is banned and alter message accordingly
                if (user.Role == Roles.Banned)
                {
                    welcomeMessage.WelcomeMessage = "You are banned from the site.";

                    commands.Add(new KeyValuePair<string, string>("Homepage", "GET api/"));
                }

                //check if user is blocked and alter message accordingly
                if (user.Role != Roles.Banned)
                {
                    welcomeMessage.WelcomeMessage = "You are blocked from the site. You can only view but not post.";

                    commands.Add(new KeyValuePair<string, string>("View posts", "GET api/posts/"));
                    commands.Add(new KeyValuePair<string, string>("View post by Id", "GET api/posts/{id}"));
                    commands.Add(new KeyValuePair<string, string>("View users", "GET api/users/"));
                    commands.Add(new KeyValuePair<string, string>("View user by Id", "GET api/users/{id}"));
                }

                //add user commands to welcomeMessage.Commands
                if (user.Role != Roles.Banned || user.Role != Roles.Blocked)
                {
                    welcomeMessage.WelcomeMessage = "Hello and welcome back to our Forum!";

                    commands.Add(new KeyValuePair<string, string>("Create post", "POST api/posts/"));
                    commands.Add(new KeyValuePair<string, string>("Edit post", "PUT api/posts/{id}"));
                    commands.Add(new KeyValuePair<string, string>("Delete post", "DELETE api/posts/{id}"));
                    commands.Add(new KeyValuePair<string, string>("Post a comment", "POST api/posts/{id}/comment/"));
                    commands.Add(new KeyValuePair<string, string>("Edit comment", "PUT api/posts/{id}/commanet/{cid}"));
                    commands.Add(new KeyValuePair<string, string>("Delete comment", "DELETE api/posts/{id}/commanet/{cid}"));
                    commands.Add(new KeyValuePair<string, string>("Upvote a post", "PUT api/posts/{id}/upvote/"));
                    commands.Add(new KeyValuePair<string, string>("Downvote a post", "PUT api/posts/{id}/downvote/"));
                    commands.Add(new KeyValuePair<string, string>("Edit user profile", "PATCH api/users/{id}"));
                    commands.Add(new KeyValuePair<string, string>("Delete user", "DELETE api/users/{id}"));
                }
                //eventually check if moderator or admin and add commands accordingly
                if (user.Role == Roles.Moderator)
                {
                }
                if (user.Role == Roles.Admin)
                {
                }

                welcomeMessage.Commands = commands;

                return StatusCode(StatusCodes.Status200OK, welcomeMessage);
            }
            catch (UnauthorizedOperationException ex)
            {
                logger.LogError(ex, "Unauthorized operation by user {0}", username);
                return StatusCode(StatusCodes.Status401Unauthorized, ex.Message);
            }
        }
    }
}
