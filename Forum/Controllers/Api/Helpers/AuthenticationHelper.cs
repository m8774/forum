﻿using Forum.Enums;
using Forum.Exceptions;
using Forum.Models;
using Forum.Services.Interfaces;

namespace Forum.Controllers.Helpers
{
    public class AuthenticationHelper
    {
        private readonly IUsersService usersService;

        public AuthenticationHelper(IUsersService usersService)
        {
            this.usersService = usersService;
        }
        public User TryGetUser(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
            {
                return null;
            }
            try
            {
                var user = this.usersService.Get(username);
                if (user.Password != password)
                {
                    throw new UnauthorizedOperationException("Wrong password");
                }
                if (user.Role == Roles.Banned)
                {
                    throw new UnauthorizedOperationException("You are banned!");
                }
                return user;
            }
            catch (EntityNotFoundException)
            {
                throw new UnauthorizedOperationException("Invalid Username");
            }
        }
    }
}
