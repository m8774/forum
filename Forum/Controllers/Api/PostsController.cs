﻿using Forum.Controllers.Helpers;
using Forum.Exceptions;
using Forum.Models;
using Forum.Models.Dtos;
using Forum.Models.Mappers;
using Forum.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Forum.Controllers.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class PostsController : ControllerBase
    {
        private readonly IPostsService postsService;
        private readonly ICommentsService commentsService;
        private readonly AuthenticationHelper authenticationHelper;
        private readonly PostMapper postMapper;

        private readonly ILogger<PostsController> logger;


        public PostsController(ILogger<PostsController> logger, IPostsService postsService, ICommentsService commentsService, AuthenticationHelper authenticationHelper, PostMapper postMapper)
        {
            this.logger = logger;
            this.postsService = postsService;
            this.commentsService = commentsService;
            this.authenticationHelper = authenticationHelper;
            this.postMapper = postMapper;
        }

        // GET: api/<PostsController>
        [HttpGet("")]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public IActionResult Get([FromHeader] string username, [FromHeader] string password, [FromQuery] PostQueryParameters filterParameters)
        {

            try
            {
                var user = authenticationHelper.TryGetUser(username, password);
                var posts = postsService.Get(filterParameters, user);

                IEnumerable<PostResponseDto> postsResponse = postMapper.ConvertToResponse(posts);
                
                Pagination metadata = new Pagination(posts.TotalPages, posts.PageNumber, posts.HasNextPage, posts.HasPreviosPage);
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));

                if (user != null)
                {
                    logger.LogInformation("Get posts requested by user {0}", user.Username);

                }
                else
                {
                    logger.LogInformation("Got top 10 posts for anonymous user");
                }

                return StatusCode(StatusCodes.Status200OK, postsResponse);
            }
            catch (UnauthorizedOperationException ex)
            {
                logger.LogError(ex, "Unauthorized operation by user {0}", username);
                return StatusCode(StatusCodes.Status401Unauthorized, ex.Message);
            }
            catch (EntityNotFoundException ex)
            {
                logger.LogError(ex, "Post with query parameters: {0} not found", filterParameters);
                return StatusCode(StatusCodes.Status404NotFound, ex.Message);
            }
            catch (ResourceUnvailableException ex)
            {
                logger.LogError(ex, "Error in the database");
                return StatusCode(StatusCodes.Status503ServiceUnavailable, ex.Message);
            }
        }

        // GET api/<PostsController>/5
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public IActionResult Get(int id, [FromHeader] string username, [FromHeader] string password)
        {
            try
            {
                var user = authenticationHelper.TryGetUser(username, password);
                IEnumerable<Post> posts = new[] { postsService.Get(id, user) };

                PostResponseDto postsResponse = postMapper.ConvertToResponse(posts).FirstOrDefault();

                logger.LogInformation("Get post with id {0} requested by user {1}", id, user.Username);
                return StatusCode(StatusCodes.Status200OK, postsResponse);
            }
            catch (UnauthorizedOperationException ex)
            {
                logger.LogError(ex, "Unauthorized operation by user {0}", username);
                return StatusCode(StatusCodes.Status401Unauthorized, ex.Message);
            }
            catch (EntityNotFoundException ex)
            {
                logger.LogError(ex, "Post with id {0} not found", id);
                return StatusCode(StatusCodes.Status404NotFound, ex.Message);
            }
            catch (ResourceUnvailableException ex)
            {
                logger.LogError(ex, "Error in the database");
                return StatusCode(StatusCodes.Status503ServiceUnavailable, ex.Message);
            }
        }

        // POST api/<PostsController>
        [HttpPost("")]
        [ProducesResponseType(201)]
        [ProducesResponseType(401)]
        [ProducesResponseType(409)]
        public IActionResult Post([FromBody] PostDto dto, [FromHeader] string username, [FromHeader] string password)
        {
            try
            {
                var user = authenticationHelper.TryGetUser(username, password);
                var post = postMapper.ConvertToModel(dto, user);
                var createdPost = postsService.Create(post, user);
                post.Tags = postsService.AddTagsToPosts(createdPost.Id, dto.Tags);

                logger.LogInformation("Post created by user {0}", user.Username);
                return StatusCode(StatusCodes.Status201Created, createdPost);
            }
            catch (UnauthorizedOperationException ex)
            {
                logger.LogError(ex, "Unauthorized operation by user {0}", username);
                return StatusCode(StatusCodes.Status401Unauthorized, ex.Message);
            }
            catch (DuplicateEntityException ex)
            {
                logger.LogError(ex, "Duplicate post creation by user {0}", username);
                return StatusCode(StatusCodes.Status409Conflict, ex.Message);
            }
        }

        // POST api/<PostsController>/5/comment
        [HttpPost("{id}/comment")]
        [ProducesResponseType(201)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]

        public IActionResult Comment(int id, [FromBody] CommentDto dto, [FromHeader] string username, [FromHeader] string password)
        {
            try
            {
                var user = authenticationHelper.TryGetUser(username, password);
                var post = postsService.Get(id, user);

                Comment newComment = new Comment { Author = user, Content = dto.Content, PostId = id, Post = post, AuthorId = user.Id, Title = $"A comment on post '{post.Title}'" };

                var createdPost = commentsService.Create(newComment, user);

                logger.LogInformation("Comment created by user {0}", user.Username);
                return StatusCode(StatusCodes.Status201Created, createdPost);
            }
            catch (UnauthorizedOperationException ex)
            {
                logger.LogError(ex, "Unauthorized operation by user {0}", username);
                return StatusCode(StatusCodes.Status401Unauthorized, ex.Message);
            }
            catch (EntityNotFoundException ex)
            {
                logger.LogError(ex, "Post with id {0} not found", id);
                return StatusCode(StatusCodes.Status404NotFound, ex.Message);
            }
        }

        // PUT api/<PostsController>/5/comment/5
        [HttpPut("{id}/comment/{cid}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public IActionResult UpdateComment(int id, int cid, [FromBody] CommentDto dto, [FromHeader] string username, [FromHeader] string password)
        {
            try
            {
                var user = authenticationHelper.TryGetUser(username, password);
                var post = postsService.Get(id, user);
                var commentToUpdate = new Comment { Author = user, Content = dto.Content, Post = post, Id = id, AuthorId = user.Id, Title = $"A comment on post '{post.Title}'" };

                var updatedPost = commentsService.Update(cid, commentToUpdate, user);

                logger.LogInformation("Comment updated by user {0}", user.Username);
                return StatusCode(StatusCodes.Status200OK, updatedPost);
            }
            catch (UnauthorizedOperationException ex)
            {
                logger.LogError(ex, "Unauthorized operation by user {0}", username);
                return StatusCode(StatusCodes.Status401Unauthorized, ex.Message);
            }
            catch (EntityNotFoundException ex)
            {
                logger.LogError(ex, "Post or comment not found");
                return StatusCode(StatusCodes.Status404NotFound, ex.Message);
            }
        }
        // DELETE api/<PostsController>/5/comment/5
        [HttpDelete("{id}/comment/{cid}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public IActionResult DeleteComment(int id, int cid, [FromHeader] string username, [FromHeader] string password)
        {
            try
            {
                var user = authenticationHelper.TryGetUser(username, password);
                var post = postsService.Get(id, user);

                commentsService.Delete(cid, user);

                logger.LogInformation("Post with id {0} deleted by user {1}", id, user.Username);
                return StatusCode(StatusCodes.Status200OK);
            }
            catch (UnauthorizedOperationException ex)
            {
                logger.LogError(ex, "Unauthorized operation by user {0}", username);
                return StatusCode(StatusCodes.Status401Unauthorized, ex.Message);
            }
            catch (EntityNotFoundException ex)
            {
                logger.LogError(ex, "Post or comment not found");
                return StatusCode(StatusCodes.Status404NotFound, ex.Message);
            }
        }

        // PUT api/<PostsController>/5
        [HttpPut("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public IActionResult Update(int id, [FromHeader] string username, [FromHeader] string password, [FromBody] PostDto dto)
        {
            try
            {
                var user = authenticationHelper.TryGetUser(username, password);
                var postToUpdate = postMapper.ConvertToModel(dto, user);
                var updatedPost = postsService.Update(id, postToUpdate, user);
                updatedPost.Tags = postsService.AddTagsToPosts(updatedPost.Id, dto.Tags);

                logger.LogInformation("Post with id {0} updated by user {1}", id, user.Username);
                return StatusCode(StatusCodes.Status200OK, updatedPost);
            }
            catch (UnauthorizedOperationException ex)
            {
                logger.LogError(ex, "Unauthorized operation by user {0}", username);
                return StatusCode(StatusCodes.Status401Unauthorized, ex.Message);
            }
            catch (EntityNotFoundException ex)
            {
                logger.LogError(ex, "Post with id {0} not found", id);
                return StatusCode(StatusCodes.Status404NotFound, ex.Message);
            }
        }

        // PUT api/<PostsController>/5/upvote
        [HttpPut("{id}/upvote")]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(409)]
        public IActionResult UpVote(int id, [FromHeader] string username, [FromHeader] string password)
        {
            try
            {
                var user = authenticationHelper.TryGetUser(username, password);
                postsService.UpVote(id, user);

                logger.LogInformation("Post with id {0} upvoted by user {1}", id, user.Username);
                return StatusCode(StatusCodes.Status200OK);
            }
            catch (UnauthorizedOperationException ex)
            {
                logger.LogError(ex, "Unauthorized operation by user {0}", username);
                return StatusCode(StatusCodes.Status401Unauthorized, ex.Message);
            }
            catch (EntityNotFoundException ex)
            {
                logger.LogError(ex, "Post with id {0} not found", id);
                return StatusCode(StatusCodes.Status404NotFound, ex.Message);
            }
            catch (DuplicateEntityException ex)
            {
                logger.LogError(ex, "Duplicate post upvote by user {0}", username);
                return StatusCode(StatusCodes.Status409Conflict, ex.Message);
            }
        }

        // PUT api/<PostsController>/5/downvote
        [HttpPut("{id}/downvote")]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        [ProducesResponseType(409)]
        public IActionResult DownVote(int id, [FromHeader] string username, [FromHeader] string password)
        {
            try
            {
                var user = authenticationHelper.TryGetUser(username, password);
                postsService.DownVote(id, user);

                logger.LogInformation("Post with id {0} downvoted by user {1}", id, user.Username);
                return StatusCode(StatusCodes.Status200OK);
            }
            catch (UnauthorizedOperationException ex)
            {
                logger.LogError(ex, "Unauthorized operation by user {0}", username);
                return StatusCode(StatusCodes.Status401Unauthorized, ex.Message);
            }
            catch (EntityNotFoundException ex)
            {
                logger.LogError(ex, "Post with id {0} not found", id);
                return StatusCode(StatusCodes.Status404NotFound, ex.Message);
            }
            catch (DuplicateEntityException ex)
            {
                logger.LogError(ex, "Duplicate post downvote by user {0}", username);
                return StatusCode(StatusCodes.Status409Conflict, ex.Message);
            }
        }

        // DELETE api/<PostsController>/5
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public IActionResult Delete(int id, [FromHeader] string username, [FromHeader] string password)
        {
            try
            {
                var user = authenticationHelper.TryGetUser(username, password);
                postsService.Delete(id, user);

                logger.LogInformation("Post with id {0} deleted by user {1}", id, user.Username);
                return StatusCode(StatusCodes.Status200OK);
            }
            catch (UnauthorizedOperationException ex)
            {
                logger.LogError(ex, "Unauthorized operation by user {0}", username);
                return StatusCode(StatusCodes.Status401Unauthorized, ex.Message);
            }
            catch (EntityNotFoundException ex)
            {
                logger.LogError(ex, "Post with id {0} not found", id);
                return StatusCode(StatusCodes.Status404NotFound, ex.Message);
            }
        }
    }
}
