﻿using Forum.Controllers.Helpers;
using Forum.Exceptions;
using Forum.Models;
using Forum.Models.Dtos;
using Forum.Models.Mappers;
using Forum.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Forum.Controllers.Api
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUsersService usersService;
        private readonly AuthenticationHelper authenticationHelper;
        private readonly UserMapper mapper;
        private readonly UserUpdateMapper updateMapper;

        private readonly ILogger<UsersController> logger;


        public UsersController(ILogger<UsersController> logger, IUsersService usersService, AuthenticationHelper authenticationHelper, UserMapper mapper, UserUpdateMapper updateMapper)
        {
            this.logger = logger;
            this.usersService = usersService;
            this.authenticationHelper = authenticationHelper;
            this.mapper = mapper;
            this.updateMapper = updateMapper;
        }
        // GET: api/<UsersController>
        [HttpGet("")]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public IActionResult Get([FromHeader] string username, [FromHeader] string password, [FromQuery] UserQueryParameters filterParameters)
        {
            try
            {
                var user = authenticationHelper.TryGetUser(username, password);
                var users = usersService.Get(filterParameters, user);

                IEnumerable<UserResponseDto> usersResponse = mapper.ConvertToResponse(users);

                Pagination metadata = new Pagination(users.TotalPages, users.PageNumber, users.HasNextPage, users.HasPreviosPage);
                Response.Headers.Add("X-Pagination", JsonConvert.SerializeObject(metadata));

                logger.LogInformation("Get all users by {0}", user.Username);

                return StatusCode(StatusCodes.Status200OK, usersResponse);
            }
            catch (UnauthorizedOperationException ex)
            {
                logger.LogError(ex, "Unauthorized operation by {0}", username);
                return StatusCode(StatusCodes.Status401Unauthorized, ex.Message);
            }
            catch (EntityNotFoundException ex)
            {
                logger.LogError(ex, "User with query parameters: {0} not found", filterParameters);
                return StatusCode(StatusCodes.Status404NotFound, ex.Message);
            }
        }

        // GET api/<UsersController>/5
        [HttpGet("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]

        public IActionResult Get(int id, [FromHeader] string username, [FromHeader] string password)
        {
            try
            {
                var user = authenticationHelper.TryGetUser(username, password);
                IEnumerable<User> returnUser = new[] { usersService.Get(id, user) };

                UserResponseDto usersResponse = mapper.ConvertToResponse(returnUser).FirstOrDefault();

                logger.LogInformation("Get user by id {0} by {1}", id, user.Username);

                return StatusCode(StatusCodes.Status200OK, usersResponse);
            }
            catch (UnauthorizedOperationException ex)
            {
                logger.LogError(ex, "Unauthorized operation by {0}", username);

                return StatusCode(StatusCodes.Status401Unauthorized, ex.Message);
            }
            catch (EntityNotFoundException ex)
            {
                logger.LogError(ex, "User with id {0} not found", id);
                return StatusCode(StatusCodes.Status404NotFound, ex.Message);
            }
        }

        // POST api/<UsersController>
        [HttpPost("")]
        [ProducesResponseType(201)]
        [ProducesResponseType(409)]
        public IActionResult Register([FromBody] UserDto dto)
        {
            try
            {
                var user = mapper.ConvertToModel(dto);

                var createdUser = usersService.Create(user);

                logger.LogInformation("Create user {0}", createdUser.Username);

                return StatusCode(StatusCodes.Status201Created, createdUser);
            }
            catch (DuplicateEntityException ex)
            {
                logger.LogError(ex, "Duplicate user {0}", dto.Username);
                return StatusCode(StatusCodes.Status409Conflict, ex.Message);
            }
        }


        // PATCH api/<UsersController>/5
        [HttpPatch("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public IActionResult Update(int id, [FromHeader] string username, [FromHeader] string password, [FromBody] UserUpdateDto dto)
        {
            try
            {
                var user = authenticationHelper.TryGetUser(username, password);
                var userToUpdate = updateMapper.ConvertToModel(dto);
                var updatedUser = usersService.Update(id, userToUpdate, user);

                logger.LogInformation("Update user by id {0} by {1}", id, user.Username);
                return StatusCode(StatusCodes.Status200OK, updatedUser);
            }
            catch (UnauthorizedOperationException ex)
            {
                logger.LogError(ex, "Unauthorized operation by {0}", username);
                return StatusCode(StatusCodes.Status401Unauthorized, ex.Message);
            }
            catch (EntityNotFoundException ex)
            {
                logger.LogError(ex, "User with id {0} not found", id);
                return StatusCode(StatusCodes.Status404NotFound, ex.Message);
            }
        }

        // DELETE api/<UsersController>/5
        [HttpDelete("{id}")]
        [ProducesResponseType(200)]
        [ProducesResponseType(401)]
        [ProducesResponseType(404)]
        public IActionResult Delete(int id, [FromHeader] string username, [FromHeader] string password)
        {
            try
            {
                var user = authenticationHelper.TryGetUser(username, password);
                usersService.Delete(id, user);

                logger.LogInformation("Delete user by id {0} by {1}", id, user.Username);
                return StatusCode(StatusCodes.Status200OK);
            }
            catch (UnauthorizedOperationException ex)
            {
                logger.LogError(ex, "Unauthorized operation by {0}", username);
                return StatusCode(StatusCodes.Status401Unauthorized, ex.Message);
            }
            catch (EntityNotFoundException ex)
            {
                logger.LogError(ex, "User with id {0} not found", id);
                return StatusCode(StatusCodes.Status404NotFound, ex.Message);
            }
        }
    }
}
