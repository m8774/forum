﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Forum.Data;
using Forum.Models;
using Forum.Models.Views;
using Forum.Enums;
using Forum.Models.Mappers;
using System.Net.Http;
using Forum.Services.Interfaces;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Forum.Models.Dtos;
using System.Text;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;

namespace Forum.Controllers
{
    public class UsersController : Controller
    {
        private readonly UserUpdateMapper updateMapper;
        private readonly IUsersService usersService;
        private readonly string appUrl;

        public UsersController(UserUpdateMapper updateMapper, IUsersService usersService, IConfiguration config)
        {
            this.updateMapper = updateMapper;
            this.usersService = usersService;
            this.appUrl = config.GetValue<string>("applicationUrl");
        }

        // GET: Users
        public async Task<IActionResult> Index(UserQueryParameters queryParameters, string searchBy = "username")
        {
            ViewData["UserSearch"] = queryParameters.SearchQuery;

            var filterParameters = new Dictionary<string, string>()
            {
                ["PageNumber"] = queryParameters.PageNumber.ToString(),
                ["PageSize"] = queryParameters.PageSize.ToString(),
                ["SortBy"] = queryParameters.SortBy.ToString(),
                ["OrderBy"] = queryParameters.OrderBy.ToString() == "true" ? "descending" : "ascending"
            };
            switch (searchBy)
            {
                case "username":
                    filterParameters["FilterByUsername"] = queryParameters.SearchQuery.ToString();
                    break;
                case "email":
                    filterParameters["FilterByEmail"] = queryParameters.SearchQuery.ToString();
                    break;
                case "firstname":
                    filterParameters["FilterByFirstName"] = queryParameters.SearchQuery.ToString();
                    break;
                default:
                    break;
            }
            ViewData["searchBy"] = searchBy;

            IEnumerable<UserResponseDto> users;
            var user = this.usersService.Get(this.HttpContext.Session.GetString("CurrentUser"));

            using (var client = new HttpClient())
            {
                //client.BaseAddress = new Uri($"{appUrl}/");
                client.DefaultRequestHeaders.Add("username", user.Username);
                client.DefaultRequestHeaders.Add("password", user.Password);

                var uri = QueryHelpers.AddQueryString($"{appUrl}/api/users", filterParameters);
                //HTTP GET
                var result = await client.GetAsync(uri);

                if (result.IsSuccessStatusCode)
                {
                    var readTask = await result.Content.ReadAsStringAsync();

                    users = JsonConvert.DeserializeObject<IList<UserResponseDto>>(readTask);

                    result.Headers.TryGetValues("X-Pagination", out IEnumerable<string> pagination);
                    var paginationHeader = JsonConvert.DeserializeObject<Pagination>(pagination.FirstOrDefault());
                    ViewBag.Pagination = paginationHeader;
                }
                else //web api sent error response
                {
                    //log response status here..

                    users = Enumerable.Empty<UserResponseDto>();

                    string error = result.Content.ReadAsStringAsync().Result;

                    ModelState.AddModelError(string.Empty, error);
                }
            }
            return View(users);
        }



        // GET: Users/Details/5
        public async Task<IActionResult> Details(int id)
        {

            var user = this.usersService.Get(this.HttpContext.Session.GetString("CurrentUser"));
            UserResponseDto userById;

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("username", user.Username);
                client.DefaultRequestHeaders.Add("password", user.Password);

                //HTTP GET
                string v = $"{appUrl}/api/users/{id}";
                var result = await client.GetAsync(v);

                if (result.IsSuccessStatusCode)
                {
                    var readTask = await result.Content.ReadAsStringAsync();

                    userById = JsonConvert.DeserializeObject<UserResponseDto>(readTask);
                }
                else //web api sent error response
                {
                    //log response status here..

                    userById = new UserResponseDto();

                    string error = result.Content.ReadAsStringAsync().Result;

                    ModelState.AddModelError(string.Empty, error);
                }
            }
            if (HttpContext.Request.Headers["Referer"].ToString().Contains("Edit"))
            {
                ViewBag.Back = "/Users/Index";
            }
            else
            {
                ViewBag.Back = HttpContext.Request.Headers["Referer"];
            }

            return View(userById);
        }

        // GET: Users/Create
        public IActionResult Create()
        {
            return View(new UserDto());
        }

        // POST: Users/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("FirstName,LastName,Username,Password,Email")] UserDto dto)
        {
            if (ModelState.IsValid)
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri($"{appUrl}/");

                    //HTTP POST
                    var result = await client.PostAsync("api/users", new StringContent(
                                    JsonConvert.SerializeObject(dto), Encoding.UTF8, "application/json"));

                    if (result.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Login", "Auth");
                    }
                    else
                    {
                        string error = result.Content.ReadAsStringAsync().Result;

                        ModelState.AddModelError(string.Empty, error);
                    }
                }
            }
            return View(dto);
        }


        // GET: Users/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            var user = this.usersService.Get(this.HttpContext.Session.GetString("CurrentUser"));
            UserResponseDto userById;

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("username", user.Username);
                client.DefaultRequestHeaders.Add("password", user.Password);

                //HTTP GET
                string v = $"{appUrl}/api/users/{id}";
                var result = await client.GetAsync(v);

                if (result.IsSuccessStatusCode)
                {
                    var readTask = await result.Content.ReadAsStringAsync();

                    userById = JsonConvert.DeserializeObject<UserResponseDto>(readTask);
                }
                else //web api sent error response
                {
                    //log response status here..

                    userById = new UserResponseDto();

                    string error = result.Content.ReadAsStringAsync().Result;

                    ModelState.AddModelError(string.Empty, error);
                }
            }

            ViewBag.Back = HttpContext.Request.Headers["Referer"];

            return View(userById);
        }

        // POST: Users/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("FirstName,LastName,Username,Email,Picture,Phone,Role,UserId,RegisteredOn")] UserResponseDto user, string password)
        {
            var currentUser = this.usersService.Get(this.HttpContext.Session.GetString("CurrentUser"));

            if (ModelState.IsValid)
            {
                var userToUpdate = updateMapper.ConvertToDto(user);
                userToUpdate.Password = password;
                using (var client = new HttpClient())
                {

                    client.DefaultRequestHeaders.Add("username", currentUser.Username);
                    client.DefaultRequestHeaders.Add("password", currentUser.Password);

                    client.BaseAddress = new Uri($"{appUrl}/");

                    //HTTP POST
                    string v = $"api/users/{id}";
                    var result = await client.PatchAsync(v, new StringContent(
                                    JsonConvert.SerializeObject(userToUpdate), Encoding.UTF8, "application/json"));

                    if (result.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Details", "Users", new { id = id });
                    }
                    else
                    {
                        string error = result.Content.ReadAsStringAsync().Result;

                        ModelState.AddModelError(string.Empty, error);
                    }
                }
            }
            return View(user);
        }

        // GET: Users/Delete/5
        public async Task<IActionResult> DeleteV(int id)
        {
            var user = this.usersService.Get(this.HttpContext.Session.GetString("CurrentUser"));
            UserResponseDto userById;

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("username", user.Username);
                client.DefaultRequestHeaders.Add("password", user.Password);

                //HTTP GET
                string v = $"{appUrl}/api/users/{id}";
                var result = await client.GetAsync(v);

                if (result.IsSuccessStatusCode)
                {
                    var readTask = await result.Content.ReadAsStringAsync();

                    userById = JsonConvert.DeserializeObject<UserResponseDto>(readTask);
                }
                else //web api sent error response
                {
                    //log response status here..

                    userById = new UserResponseDto();

                    string error = result.Content.ReadAsStringAsync().Result;

                    ModelState.AddModelError(string.Empty, error);
                }
            }

            ViewBag.Back = HttpContext.Request.Headers["Referer"];

            return View(userById);
        }

        // POST: Users/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int UserId)
        {
            var user = this.usersService.Get(this.HttpContext.Session.GetString("CurrentUser"));

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri($"{appUrl}/");
                client.DefaultRequestHeaders.Add("username", user.Username);
                client.DefaultRequestHeaders.Add("password", user.Password);

                //HTTP DELETE
                string v = $"api/users/{UserId}";
                var result = await client.DeleteAsync(v);

                if (result.IsSuccessStatusCode)
                {
                    if (this.HttpContext.Session.GetString("CurrentUserId") == UserId.ToString())
                    {
                    return RedirectToAction("Logout", "Auth");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Users");
                    }
                }
                else
                {
                    string error = result.Content.ReadAsStringAsync().Result;

                    ModelState.AddModelError(string.Empty, error);
                }
            }

            if (ViewBag.Back.ToString() == "Users")
            {
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return RedirectToAction(nameof(Details), new { id = UserId });
            }
        }

        //private bool UserExists(int id)
        //{
        //    return _context.Users.Any(e => e.Id == id);
        //}
        //private void InitializeDropDownLists(UserViewModel viewModel)
        //{
        //    var roleList = from Roles r in Enum.GetValues(typeof(Roles))
        //                     select new { ID = (int)r, Name = r.ToString() };
        //    viewModel.RolesList = new SelectList(roleList, "Id", "Name");
        //}
    }
}
