﻿using Forum.Controllers.Helpers;
using Forum.Exceptions;
using Forum.Models.Views;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Forum.Controllers
{
    public class AuthController : Controller
    {
        private readonly AuthenticationHelper authenticationHelper;

        public AuthController(AuthenticationHelper authenticationHelper)
        {
            this.authenticationHelper = authenticationHelper;

        }
        public IActionResult Login()
        {
            var login = new LoginViewModel();
            return View(login);
        }

        public IActionResult Register()
        {
            return RedirectToAction("Create", "Users");
        }

        public IActionResult Logout()
        {
            HttpContext.Session.Clear();
            return RedirectToAction("Index", "Home");
        }

        public IActionResult AccessDenied()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login([Bind("Username, Password")] LoginViewModel loginUser)
        {
            if (!ModelState.IsValid)
            {
                return View(loginUser);
            }
            try
            {
                var user = authenticationHelper.TryGetUser(loginUser.Username, loginUser.Password);

                this.HttpContext.Session.SetString("CurrentUser", user.Username);
                this.HttpContext.Session.SetString("CurrentUserRole", user.Role.ToString());
                this.HttpContext.Session.SetString("CurrentUserId", user.Id.ToString());
                return RedirectToAction("Index", "Home");
            }
            catch (UnauthorizedOperationException ex)
            {
                //logger.LogError(ex, "Unauthorized operation by user {0}", username);
                ModelState.AddModelError(string.Empty, ex.Message);
                return View(loginUser);
            }
        }
    }
}
