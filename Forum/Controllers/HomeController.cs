﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Forum.Models;
using Forum.Models.Views;
using System.Net.Http;
using Microsoft.AspNetCore.WebUtilities;
using Newtonsoft.Json;
using Forum.Models.Dtos;
using Forum.Services.Interfaces;
using Microsoft.Extensions.Configuration;

namespace Forum.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IPostsService postsService;
        private readonly IUsersService usersService;
        private readonly string appUrl;

        public HomeController(ILogger<HomeController> logger, IUsersService usersService, IPostsService postsService, IConfiguration config)
        {
            _logger = logger;
            this.postsService = postsService;
            this.usersService = usersService;
            this.appUrl = config.GetValue<string>("ApplicationUrl");
        }

        public async Task<IActionResult> Index()
        {
            ViewData["UsersCount"] = this.usersService.GetCount();
            ViewData["PostsCount"] = this.postsService.GetCount();
            
            var filterParameters = new Dictionary<string, string>()
            {
                ["PageNumber"] = "1",
                ["PageSize"] = "10",
                ["SortBy"] = "comments",
                ["OrderBy"] = "descending"
            };

            IEnumerable<PostResponseDto> postsByComments;

            using (var client = new HttpClient())
            {
                var uri = QueryHelpers.AddQueryString($"{appUrl}/api/posts", filterParameters);
                //HTTP GET
                var result = await client.GetAsync(uri);

                if (result.IsSuccessStatusCode)
                {
                    var readTask = await result.Content.ReadAsStringAsync();

                    postsByComments = JsonConvert.DeserializeObject<IList<PostResponseDto>>(readTask);
                }
                else //web api sent error response
                {
                    //log response status here..

                    postsByComments = Enumerable.Empty<PostResponseDto>();

                    string error = result.Content.ReadAsStringAsync().Result;

                    ModelState.AddModelError(string.Empty, error);
                }
            }


            filterParameters["SortBy"] = "created";

            IEnumerable<PostResponseDto> postsByDate;

            using (var client = new HttpClient())
            {
                var uri = QueryHelpers.AddQueryString($"{appUrl}/api/posts", filterParameters);
                //HTTP GET
                var result = await client.GetAsync(uri);

                if (result.IsSuccessStatusCode)
                {
                    var readTask = await result.Content.ReadAsStringAsync();

                    postsByDate = JsonConvert.DeserializeObject<IList<PostResponseDto>>(readTask);
                }

                else //web api sent error response
                {
                    //log response status here..

                    postsByDate = Enumerable.Empty<PostResponseDto>();

                    string error = result.Content.ReadAsStringAsync().Result;

                    ModelState.AddModelError(string.Empty, error);
                }
            }
            var tupleModel = new Tuple<IEnumerable<PostResponseDto>, IEnumerable<PostResponseDto>>(postsByComments, postsByDate);

            return View(tupleModel);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult Privacy()
        {
            return View();
        }
    }
}
