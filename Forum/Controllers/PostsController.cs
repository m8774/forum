﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Forum.Data;
using Forum.Models;
using System.Web;
using Microsoft.AspNetCore.Http;
using Forum.Models.Dtos;
using Forum.Models.Mappers;
using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using Forum.Services.Interfaces;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;

namespace Forum.Controllers
{
    public class PostsController : Controller
    {

        private readonly PostMapper postMapper;
        private readonly IUsersService usersService;
        private readonly string appUrl;

        public PostsController(ApplicationContext context, PostMapper postMapper, IUsersService usersService, IConfiguration config)
        {
            this.postMapper = postMapper;
            this.usersService = usersService;
            this.appUrl = config.GetValue<string>("applicationUrl");
        }

        // GET: Posts
        public async Task<IActionResult> Index(PostQueryParameters queryParameters)
        {
            queryParameters.SearchQuery ??= "";

            var filterParameters = new Dictionary<string, string>()
            {
                ["PageNumber"] = queryParameters.PageNumber.ToString(),
                ["PageSize"] = queryParameters.PageSize.ToString(),
                ["SortBy"] = queryParameters.SortBy.ToString(),
                ["FilterByUsername"] = queryParameters.FilterByUsername.ToString(),
                ["FilterByTags"] = queryParameters.FilterByTags.ToString(),
                ["SearchQuery"] = queryParameters.SearchQuery.ToString(),
                ["OrderBy"] = queryParameters.OrderBy.ToString()
            };

            IEnumerable<PostResponseDto> posts;
            var user = this.usersService.Get(this.HttpContext.Session.GetString("CurrentUser"));

            using (var client = new HttpClient())
            {
                //client.BaseAddress = new Uri($"{appUrl}/");
                client.DefaultRequestHeaders.Add("username", user.Username);
                client.DefaultRequestHeaders.Add("password", user.Password);

                var uri = QueryHelpers.AddQueryString($"{appUrl}/api/posts", filterParameters);
                //HTTP GET
                var result = await client.GetAsync(uri);

                if (result.IsSuccessStatusCode)
                {
                    var readTask = await result.Content.ReadAsStringAsync();

                    posts = JsonConvert.DeserializeObject<IList<PostResponseDto>>(readTask);

                    result.Headers.TryGetValues("X-Pagination", out IEnumerable<string> pagination);
                    var paginationHeader = JsonConvert.DeserializeObject<Pagination>(pagination.FirstOrDefault());
                    ViewBag.Pagination = paginationHeader;
                }
                else //web api sent error response
                {
                    //log response status here..

                    posts = Enumerable.Empty<PostResponseDto>();

                    string error = result.Content.ReadAsStringAsync().Result;

                    ModelState.AddModelError(string.Empty, error);
                }
            }
            return View(posts);
        }

        // GET: Posts/Details/5
        public async Task<IActionResult> Details(int id)
        {
            var user = this.usersService.Get(this.HttpContext.Session.GetString("CurrentUser"));
            PostResponseDto postById;

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("username", user.Username);
                client.DefaultRequestHeaders.Add("password", user.Password);

                //HTTP GET
                string v = $"{appUrl}/api/posts/{id}";
                var result = await client.GetAsync(v);

                if (result.IsSuccessStatusCode)
                {
                    var readTask = await result.Content.ReadAsStringAsync();

                    postById = JsonConvert.DeserializeObject<PostResponseDto>(readTask);
                }
                else //web api sent error response
                {
                    //log response status here..

                    postById = new PostResponseDto();

                    string error = result.Content.ReadAsStringAsync().Result;

                    ModelState.AddModelError(string.Empty, error);
                }
            }
            if (HttpContext.Request.Headers["Referer"].ToString().Contains("Edit"))
            {
                ViewBag.Back = "/Posts/Index";
            }
            else
            {
                ViewBag.Back = HttpContext.Request.Headers["Referer"];
            }

            return View(postById);
        }

        // POST: Posts/Upvote/5
        public async Task<IActionResult> UpvoteV(int id)
        {
            var user = this.usersService.Get(this.HttpContext.Session.GetString("CurrentUser"));

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri($"{appUrl}/");
                client.DefaultRequestHeaders.Add("username", user.Username);
                client.DefaultRequestHeaders.Add("password", user.Password);

                //HTTP PUT
                string v = $"api/posts/{id}/upvote";
                var result = await client.PutAsync(v, new StringContent("", Encoding.UTF8, "application/json"));

                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    string error = result.Content.ReadAsStringAsync().Result;

                    ModelState.AddModelError(string.Empty, error);
                }
            }

            if (ViewBag.Back.ToString() == "Posts")
            {
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return RedirectToAction(nameof(Details), new { id = id });
            }
        }

        // POST: Posts/Downvote/5
        public async Task<IActionResult> DownvoteV(int id)
        {
            var user = this.usersService.Get(this.HttpContext.Session.GetString("CurrentUser"));

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri($"{appUrl}/");
                client.DefaultRequestHeaders.Add("username", user.Username);
                client.DefaultRequestHeaders.Add("password", user.Password);

                //HTTP PUT
                string v = $"api/posts/{id}/downvote";
                var result = await client.PutAsync(v, new StringContent("", Encoding.UTF8, "application/json"));

                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    string error = result.Content.ReadAsStringAsync().Result;
                    ModelState.AddModelError(string.Empty, error);
                }
            }

            if (ViewBag.Back.ToString() == "Posts")
            {
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return RedirectToAction(nameof(Details), new { id = id });
            }
        }

        // POST: Posts/Comment
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Comment([Bind("PostId")] int PostId, [Bind("commentText")] string commentText)
        {
            if (!string.IsNullOrEmpty(commentText) && commentText.Length > 32 && commentText.Length < 8192)
            {
                var user = this.usersService.Get(this.HttpContext.Session.GetString("CurrentUser"));
                CommentDto comment = new CommentDto()
                {
                    Content = commentText
                };
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri($"{appUrl}/");
                    client.DefaultRequestHeaders.Add("username", user.Username);
                    client.DefaultRequestHeaders.Add("password", user.Password);

                    //HTTP POST
                    var result = await client.PostAsync($"api/posts/{PostId}/comment", new StringContent(
                                    JsonConvert.SerializeObject(comment), Encoding.UTF8, "application/json"));

                    if (result.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Details", "Posts", new { id = PostId });
                    }
                    else
                    {
                        string error = result.Content.ReadAsStringAsync().Result;

                        ModelState.AddModelError(string.Empty, error);
                    }
                }
            }
            else
            {
                TempData["CommentError"] = "Comment must be between 32 and 8192 characters long.";

                //ModelState.AddModelError("CommentError", "Comment must be between 32 and 8192 characters long.");
            }
            return RedirectToAction("Details", "Posts", new { id = PostId });
        }
        // GET: Posts/Create
        public IActionResult Create()
        {
            //ViewData["AuthorId"] = new SelectList(_context.Users, "Id", "Id");
            return View(new PostViewDto());
        }

        // POST: Posts/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Title,Content,Tags")] PostViewDto dto)
        {
            if (ModelState.IsValid)
            {
                var tags = dto.Tags != null ? dto.Tags.Split(',').ToList().Select(t => new TagDto() { Tag = t.Trim() }).ToList() : new List<TagDto>();
                var user = this.usersService.Get(this.HttpContext.Session.GetString("CurrentUser"));
                PostDto post = new PostDto()
                {
                    Title = dto.Title,
                    Content = dto.Content,
                    Tags = tags
                };
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri($"{appUrl}/");
                    client.DefaultRequestHeaders.Add("username", user.Username);
                    client.DefaultRequestHeaders.Add("password", user.Password);

                    //HTTP POST
                    var result = await client.PostAsync("api/posts", new StringContent(
                                    JsonConvert.SerializeObject(post), Encoding.UTF8, "application/json"));

                    if (result.IsSuccessStatusCode)
                    {
                        return RedirectToAction(nameof(Index));
                    }
                    else
                    {
                        string error = result.Content.ReadAsStringAsync().Result;

                        ModelState.AddModelError(string.Empty, error);
                    }
                }
            }
            return View(dto);
        }

        // GET: Posts/Edit/5
        public async Task<IActionResult> Edit(int id)
        {
            var user = this.usersService.Get(this.HttpContext.Session.GetString("CurrentUser"));
            PostResponseDto postById;

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("username", user.Username);
                client.DefaultRequestHeaders.Add("password", user.Password);

                //HTTP GET
                string v = $"{appUrl}/api/posts/{id}";
                var result = await client.GetAsync(v);

                if (result.IsSuccessStatusCode)
                {
                    var readTask = await result.Content.ReadAsStringAsync();

                    postById = JsonConvert.DeserializeObject<PostResponseDto>(readTask);
                }
                else //web api sent error response
                {
                    //log response status here..

                    postById = new PostResponseDto();

                    string error = result.Content.ReadAsStringAsync().Result;

                    ModelState.AddModelError(string.Empty, error);
                }
            }
            if (HttpContext.Request.Headers["Referer"].ToString().Contains("Edit"))
            {
                ViewBag.Back = "/Posts/Index";
            }
            else
            {
                ViewBag.Back = HttpContext.Request.Headers["Referer"];
            }

            PostViewEditDto postToEdit = new PostViewEditDto()
            {
                PostId = postById.PostId,
                Title = postById.Title,
                Content = postById.Content,
                AuthorId = postById.AuthorId,
                AuthorUsername = postById.AuthorUsername,
                UpVotes = postById.UpVotes,
                DownVotes = postById.DownVotes,
                Comments = postById.Comments,
                CreatedOn = postById.CreatedOn,
                ModifiedOn = postById.ModifiedOn,
                Tags = string.Join(", ", postById.Tags.Select(t => t.Tag))
            };
            return View(postToEdit);

        }

        // POST: Posts/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int PostId, [Bind("UpVotes,DownVotes,Title,Content,AuthorId,AuthorUsername,Comments,PostId,CreatedOn,ModifiedOn,Tags")] PostViewEditDto dto)
        {
            if (ModelState.IsValid)
            {
                var tags = dto.Tags != null ? dto.Tags.Split(',').ToList().Select(t => new TagDto() { Tag = t.Trim() }).ToList() : new List<TagDto>();
                var user = this.usersService.Get(this.HttpContext.Session.GetString("CurrentUser"));
                PostDto post = new PostDto()
                {
                    Title = dto.Title,
                    Content = dto.Content,
                    Tags = tags
                };
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri($"{appUrl}/");
                    client.DefaultRequestHeaders.Add("username", user.Username);
                    client.DefaultRequestHeaders.Add("password", user.Password);

                    //HTTP PUT
                    var result = await client.PutAsync($"api/posts/{PostId}", new StringContent(
                                    JsonConvert.SerializeObject(post), Encoding.UTF8, "application/json"));

                    if (result.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Details", "Posts", new { id = PostId });
                    }
                    else
                    {
                        string error = result.Content.ReadAsStringAsync().Result;

                        ModelState.AddModelError(string.Empty, error);
                    }
                }
            }
            return View(dto);
        }

        // GET: Posts/Delete/5
        public async Task<IActionResult> DeleteV(int id)
        {
            var user = this.usersService.Get(this.HttpContext.Session.GetString("CurrentUser"));
            PostResponseDto postById;

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("username", user.Username);
                client.DefaultRequestHeaders.Add("password", user.Password);

                //HTTP GET
                string v = $"{appUrl}/api/posts/{id}";
                var result = await client.GetAsync(v);

                if (result.IsSuccessStatusCode)
                {
                    var readTask = await result.Content.ReadAsStringAsync();

                    postById = JsonConvert.DeserializeObject<PostResponseDto>(readTask);
                }
                else //web api sent error response
                {
                    //log response status here..

                    postById = new PostResponseDto();

                    string error = result.Content.ReadAsStringAsync().Result;

                    ModelState.AddModelError(string.Empty, error);
                }
            }
            if (HttpContext.Request.Headers["Referer"].ToString().Contains("Edit"))
            {
                ViewBag.Back = "/Posts/Index";
            }
            else
            {
                ViewBag.Back = HttpContext.Request.Headers["Referer"];
            }

            return View(postById);

        }

        // POST: Posts/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int PostId)
        {
            var user = this.usersService.Get(this.HttpContext.Session.GetString("CurrentUser"));

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri($"{appUrl}/");
                client.DefaultRequestHeaders.Add("username", user.Username);
                client.DefaultRequestHeaders.Add("password", user.Password);

                //HTTP DELETE
                string v = $"api/posts/{PostId}";
                var result = await client.DeleteAsync(v);

                if (result.IsSuccessStatusCode)
                {
                    return RedirectToAction(nameof(Index));
                }
                else
                {
                    string error = result.Content.ReadAsStringAsync().Result;

                    ModelState.AddModelError(string.Empty, error);
                }
            }

            if (ViewBag.Back.ToString() == "Posts")
            {
                return RedirectToAction(nameof(Index));
            }
            else
            {
                return RedirectToAction(nameof(Details), new { id = PostId });
            }
        }

        // GET: Posts/5/Comment/5
        public async Task<IActionResult> EditComment(int id, int cid)
        {
            var user = this.usersService.Get(this.HttpContext.Session.GetString("CurrentUser"));
            PostResponseDto postById;

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add("username", user.Username);
                client.DefaultRequestHeaders.Add("password", user.Password);

                //HTTP GET
                string v = $"{appUrl}/api/posts/{id}";
                var result = await client.GetAsync(v);

                if (result.IsSuccessStatusCode)
                {
                    var readTask = await result.Content.ReadAsStringAsync();

                    postById = JsonConvert.DeserializeObject<PostResponseDto>(readTask);
                }
                else //web api sent error response
                {
                    //log response status here..

                    postById = new PostResponseDto();

                    string error = result.Content.ReadAsStringAsync().Result;

                    ModelState.AddModelError(string.Empty, error);
                }
            }
            if (HttpContext.Request.Headers["Referer"].ToString().Contains("Edit"))
            {
                ViewBag.Back = "/Posts/Index";
            }
            else
            {
                ViewBag.Back = HttpContext.Request.Headers["Referer"];
            }

            CommentResponseDto commentToEdit = new CommentResponseDto()
            {
                PostId = postById.PostId,
                CommentId = cid,
                Content = postById.Comments.Where(c => c.CommentId == cid).First().Content
            };
            return View(commentToEdit);
        }


        // POST: Posts/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> EditComment([Bind("Content,CommentId,PostId")] CommentResponseDto dto)
        {
            if (ModelState.IsValid)
            {
                var user = this.usersService.Get(this.HttpContext.Session.GetString("CurrentUser"));
                CommentDto comment = new CommentDto()
                {
                    Content = dto.Content
                };
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri($"{appUrl}/");
                    client.DefaultRequestHeaders.Add("username", user.Username);
                    client.DefaultRequestHeaders.Add("password", user.Password);

                    //HTTP PUT
                    var result = await client.PutAsync($"api/posts/{dto.PostId}/comment/{dto.CommentId}", new StringContent(
                                    JsonConvert.SerializeObject(comment), Encoding.UTF8, "application/json"));

                    if (result.IsSuccessStatusCode)
                    {
                        return RedirectToAction("Details", "Posts", new { id = dto.PostId });
                    }
                    else
                    {
                        string error = result.Content.ReadAsStringAsync().Result;
                        
                        ModelState.AddModelError(string.Empty, error);
                    }
                }
            }
            return View(dto);
        }

        // GET: Posts/5/Comment/5
        public async Task<IActionResult> DeleteCommentV(int id, int cid)
        {
            var user = this.usersService.Get(this.HttpContext.Session.GetString("CurrentUser"));

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri($"{appUrl}/");
                client.DefaultRequestHeaders.Add("username", user.Username);
                client.DefaultRequestHeaders.Add("password", user.Password);

                //HTTP DELETE
                string v = $"api/posts/{id}/comment/{cid}";
                var result = await client.DeleteAsync(v);

                if (!result.IsSuccessStatusCode)
                {
                    string error = result.Content.ReadAsStringAsync().Result;

                    ModelState.AddModelError(string.Empty, error);
                }
            }
            return RedirectToAction(nameof(Details), new { id = id });
        }
        //private bool PostExists(int id)
        //{
        //    return _context.Posts.Any(e => e.Id == id);
        //}
    }
}
