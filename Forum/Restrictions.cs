public class Restrictions
{
    public const int NameMinLen = 4, NameMaxLen = 32;
    public const int TitleMinLen = 16, TitleMaxLen = 64;
    public const int ContentMinLen = 32, ContentMaxLen = 8192;
}
